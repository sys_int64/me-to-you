from django.contrib import admin
from .models import Review

class ReviewAdmin (admin.ModelAdmin):
    list_display      = ('task_title', 'user_title', 'date_time', 'approved')
    raw_id_fields     = ('user', 'task')
    list_filter       = ['date_time']
    search_fields     = ['text', 'task__title', 'user__first_name', 'user__last_name']

    def task_title (self, obj):
        return obj.task.title

    def user_title (self, obj):
        return obj.user.first_name+" "+obj.user.last_name

    task_title.short_description = 'Задача'
    task_title.admin_order_field = 'task__title'

    user_title.short_description = 'Пользователь'
    user_title.admin_order_field = 'user__first_name'

admin.site.register (Review, ReviewAdmin)
