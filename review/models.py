
from django.db import models
from django.contrib.auth.models import User
from task.models import Task
import datetime
from django.utils import formats

class Review (models.Model):

    class Meta:
        verbose_name        = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    def serialize (self):
        return {
            'id'        : self.id,
            'date_time' : formats.date_format (self.date_time, "DATETIME_FORMAT"),
            'message'   : self.text,
            'user'      : {
                'id'       : self.user.id,
                'rank'     : self.rank,
                'avatar'   : self.user.userprofile.get_avatar_url(),
                'fullname' : self.user.first_name+" "+self.user.last_name,
            },
            'task'      : {
                'id'       : self.task.id,
                'title'    : self.task.title,
            },
        }

    user      = models.ForeignKey    (User, related_name = "review_owner")
    to        = models.ForeignKey    (User, related_name = "review_to")
    task      = models.ForeignKey    (Task)
    rank      = models.IntegerField  ("Рейтинг"     , default = 1)
    text      = models.TextField     ("Подробнее"   , blank   = True)
    approved  = models.BooleanField  ("Одобрен"     , default = True)
    date_time = models.DateTimeField ("Время и дата", blank   = True, default = datetime.datetime.now)
