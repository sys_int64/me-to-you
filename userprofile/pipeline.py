import datetime
from .models import UserProfile

def save_profile (backend, user, response, *args, **kwargs):

    # Facebook

    if (backend.name == 'facebook'):

        if (response.get('gender') and user.userprofile.gender == "0"):
            if   (response.get('gender') == "мужской"): user.userprofile.gender = "m"
            elif (response.get('gender') == "женский"): user.userprofile.gender = "f"

        if (response.get('location') and not user.userprofile.city):
            user.userprofile.city = response.get('location').get('name')

        if (response.get('picture')):
            if (not user.userprofile.avatar and not user.userprofile.avatar_url):
                user.userprofile.avatar_url = response.get('picture').get('data').get('url')

        user.userprofile.social = True
        user.userprofile.save()

        user.email = ""
        user.save()

    # Vkontakte

    elif (backend.name == 'vk-oauth2'):

        if (response.get('sex') and user.userprofile.gender == "0"):
            if   (response.get('sex') == 1): user.userprofile.gender = "f"
            elif (response.get('sex') == 2): user.userprofile.gender = "m"

        if (response.get('city') and not user.userprofile.city):
                user.userprofile.city = response.get('city').get('title')

        if (response.get('photo_200')):
            if (not user.userprofile.avatar and not user.userprofile.avatar_url):
                user.userprofile.avatar_url = response.get('photo_200')

        user.userprofile.social = True
        user.userprofile.save()

        user.email = ""
        user.save()

    # Google+

    elif (backend.name == 'google-oauth2'):

        if (response.get('gender') and user.userprofile.gender == "0"):
            if   (response.get('gender') == "male"  ): user.userprofile.gender = "m"
            elif (response.get('gender') == "female"): user.userprofile.gender = "f"

        if (response.get('image')):
            if (not user.userprofile.avatar and not user.userprofile.avatar_url):
                user.userprofile.avatar_url = response.get('image').get('url')

        user.userprofile.social = True
        user.userprofile.save()

        user.email = ""
        user.save()
