from django.contrib.auth.models import User
from django.db import models
from datetime import datetime
from task.models import Task
from review.models import Review
from django.utils import formats
from django.db.models import Q
from sorl.thumbnail import ImageField

GENDER_CHOICES = (
    ('0', 'Незадан'),
    ('m', 'Мужской'),
    ('f', 'Женский'),
)

TYPE_CHOICES = (
    ('0', 'Не задан'),
    ('1', 'Заказчик'),
    ('2', 'Исполнитель'),
)

class UserProfile (models.Model):
    user          = models.OneToOneField (User)
    showed_tasks  = models.ManyToManyField (Task, blank = True, related_name = "showed_tasks")
    old_tasks     = models.ManyToManyField (Task, blank = True, related_name = "old_tasks")
    birthdate     = models.DateField     ("Дата рождения" , blank   = True, default    = "1800-01-01")
    gender        = models.CharField     ("Пол"           , blank   = True, max_length = 1  , choices = GENDER_CHOICES, default = '0')
    city          = models.CharField     ("Город"         , blank   = True, max_length = 200, default = "")
    skills        = models.TextField     ("Навыки"        , blank   = True, default    = "")
    avatar_url    = models.CharField     ("Аватар"        , blank   = True, max_length = 255)
    avatar        = ImageField           ("Аватар"        , blank   = True)
    phone         = models.CharField     ("Телефон"       , blank   = True, max_length = 30)
    new_phone     = models.CharField     ("Новый Телефон" , blank   = True, max_length = 30)
    utype         = models.CharField     ("Тип"           , blank   = True, max_length = 1  , choices = TYPE_CHOICES, default = '0')
    secret        = models.CharField     ("Secret"        , blank   = True, max_length = 10)
    secret_change = models.DateTimeField ("Secret changed", blank   = True, default = datetime.now)
    active        = models.BooleanField  ("Активирован"   , default = False)
    social        = models.BooleanField  ("Соц сети"      , default = False)
    accept_phone  = models.BooleanField  (                  default = False)
    accept_passwd = models.BooleanField  (                  default = False)
    restart_shows = models.BooleanField  (                  default = False)
    rank          = models.IntegerField  ("Ранг"          , default = 0)
    tasks_count   = models.IntegerField  ("Задачи"        , default = 0)
    tasks_count2  = models.IntegerField  ("Задачи"        , default = 0)
    new_passwd    = models.CharField     (                  blank   = True, max_length = 30)
    gcm_token     = models.CharField     ("GCM TOKEN"     , blank   = True, max_length = 255)
    tasks_shows   = models.IntegerField  ("Просмотренно заданий", default = 0)
    end_tariff    = models.DateTimeField ("Заканчивается тариф", blank = True, default = datetime.now)

    # Получение url аватара

    def get_avatar_url (self):
        if (self.avatar): return "{0}{1}".format ("http://me2you.pro", self.avatar.url)
        else:             return self.avatar_url

    # Сериализация

    def serialize (self, extend = False):
        monthes = ["января", "февраля", "марта"   , "апреля" , "мая"   , "июня",
                   "июля"  , "августа", "сентября", "октября", "ноября", "декабря"]

        self.update_rank()
        self.update_task_count()
        self.update_task_count2()

        my_reviews       = Review.objects.filter (approved = True, user = self.user).count()
        about_me_reviews = Review.objects.filter (approved = True, to   = self.user).count()

        return {
            'id'               : self.user.id,
            'email'            : self.user.email,
            'firstname'        : self.user.first_name,
            'lastname'         : self.user.last_name,
            'avatar'           : self.get_avatar_url(),
            'gender'           : self.gender,
            'city'             : self.city,
            'skills'           : self.skills,
            'birthdate'        : formats.date_format (self.birthdate, "DATE_FORMAT"),
            'active'           : self.active,
            'phone'            : self.phone,
            'type'             : int(self.utype),
            'rank'             : self.rank,
            'start'            : 0,
            'social'           : self.social,
            'tasks_count'      : self.tasks_count,
            'tasks_count2'     : self.tasks_count2,
            'date_joined'      : monthes[self.user.date_joined.month-1]+" "+str(self.user.date_joined.year),
            'my_reviews'       : my_reviews,
            'about_me_reviews' : about_me_reviews,
        }

    # Сериализация с не большим количеством полей

    def _serialize_short (self):
        pass

    # Сериализация расширенная, со всеми полями

    def _serialize_extend (self):
        pass

    # Обновление ранга пользователя

    def update_rank (self):
        reviews = Review.objects.filter (approved = True, to = self.user)
        sum_ranks   = 0
        count_ranks = reviews.count()

        for review in reviews:
            sum_ranks += review.rank

        if (count_ranks == 0):
            self.rank = 0
            self.save()
            return

        srank = float (sum_ranks) / float (count_ranks)
        rank  = round (srank)

        self.rank = rank
        self.save()

    # Обновление количества заданий у пользователя

    def update_task_count (self):

        if (self.utype == "1"):
            tasks_count = Task.objects.filter (Q(user = self.user) & (Q(status = "6") | Q(status = "2") | Q(status = "3") | Q(status = "5"))).count()
        else:
            tasks_count = Task.objects.filter (perfomer = self.user, status = "6").count()

        self.tasks_count = tasks_count
        self.save()

    def update_task_count2 (self):

        tasks_count = Task.objects.filter (Q(user = self.user) & (Q(status = "6") | Q(status = "2") | Q(status = "3") | Q(status = "5"))).count()
        self.tasks_count2 = tasks_count
        self.save()

    # Событие на сохранение профиля, привязывает профиль к пользователю

    def save (self, *args, **kwargs):
        if not self.pk:
            try:
                p = UserProfile.objects.get(user=self.user)
                self.pk = p.pk
            except UserProfile.DoesNotExist:
                pass

        super(UserProfile, self).save(*args, **kwargs)

from django.db.models.signals import post_save

# Отслеживание сигнала

def create_profile (sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        profile = UserProfile (user = user)
        profile.save()

post_save.connect (create_profile, sender = User)
