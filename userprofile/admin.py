from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import UserProfile
from task.models import Category

class UserProfileInline (admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'userprofile'

class UserCategory (admin.StackedInline):
    model   = Category
    extra   = 0

class UserAdmin (UserAdmin):

    def full_name (self, obj):
        name = obj.first_name+" "+obj.last_name

        if name.strip() == "":
            return obj.username

        return name

    def social (self, obj):
        return obj.userprofile.social

    full_name.allow_tags = True
    full_name.short_description = "Полное имя"

    social.boolean = True
    social.short_description = "Социальная сеть"

    list_display = ("full_name", "email", "social")
    inlines = (UserProfileInline, UserCategory, )

admin.site.site_header = "Администрирование Me2You"
admin.site.unregister (User)
admin.site.register   (User, UserAdmin)
