from django.db import connection
from time import time
from operator import add
import re

from userprofile.models import UserProfile
import datetime

class UpdateShowsMiddleware(object):

    def process_view(self, request, view_func, view_args, view_kwargs):
        response = view_func(request, *view_args, **view_kwargs)

        today = datetime.date.today()

        if (today.weekday() > 0):
            UserProfile.objects.filter (restart_shows = True).update(restart_shows = False)

        if (today.weekday() == 0):
            UserProfile.objects.filter (restart_shows = False).update(restart_shows = True, tasks_shows = 0)

        return response
