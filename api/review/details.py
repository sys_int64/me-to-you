"""

@api {get} /reviews[:offset,:limit]/ Отобразить отзывы пользователя
@apiName Reviews
@apiGroup Review
@apiDescription

Поля `:offset`; `:limit` являются не обязательными.\
Отображает уведомления для пользователя с идентификатором `user_id`.

@apiParam {Number} user_id Идентификатор пользователя, если `user_id` = `my`, то отобразить пользователя по авторизованному токену
@apiParam {Boolean} about_me Отобразить отзывы о пользователе, если `true`
@apiParam {[Review]} reviews Отзывы пользователя
@apiParam {Number} id Идентификатор пользователя
@apiParam {String} date_time Дата и время отзыва
@apiParam {String} message Отзыв
@apiParam {[User]} user Краткое содержание <a href = "#api-User-GetUser">пользователя</a>, отображается только поля `id`; `rank`; `fullname`; `avatar`
                        поле `fullname` = `first_name`+" "+`last_name`. Либо `null`, если пользователь не указан.
@apiSuccess {[Task]}  task Краткое содержание <a href = "#api-Task-GetTask">задачи</a>, отображается только поля `id`; `title`.
                        Либо `null`, если пользователь не указан.

@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.db.models               import Q
from review.models                  import Review

class ReviewsView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):
        user_id  = request.GET.get ("user_id");
        about_me = request.GET.get ("about_me");

        if (user_id == "my"):
            if (not request.user.id):
                return Response ({"error": "U0"})

            user = request.user
        else:
            try:
                user = User.objects.get (pk = int(user_id))

            except User.DoesNotExist:
                return Response ({"error": "U0"})

        if (about_me == "1"):
            #query = Q (approved = True, task__user = user)
            #if (user.userprofile.utype == "1"):
            query = Q (to = user)

            #if (user.userprofile.utype == "2"):
            #    query = Q (task__perfomer = user)
        else:
            query = Q (user = user)

        reviews = Review.objects.filter (query).order_by ("-date_time")#[int(offset):int(offset)+int(limit)];
        reviews_content = []

        for review in reviews:
            reviews_content.append (review.serialize())

        return Response ({"reviews": reviews_content})


class ReviewsAllView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format=None):
        user_id  = request.GET.get ("user_id");
        about_me = request.GET.get ("about_me");

        if (user_id == "my"):
            if (not request.user.id):
                return Response ({"error": "U0"})

            user = request.user
        else:
            try:
                user = User.objects.get (pk = int(user_id))

            except User.DoesNotExist:
                return Response ({"error": "U0"})

        if (about_me == "1"):
            #query = Q (approved = True, task__user = user)
            #if (user.userprofile.utype == "1"):
            query = Q (to = user)

            #if (user.userprofile.utype == "2"):
            #    query = Q (task__perfomer = user)
        else:
            query = Q (user = user)

        reviews = Review.objects.filter (query).order_by ("-date_time")#[int(offset):int(offset)+int(limit)];
        reviews_content = []

        for review in reviews:
            reviews_content.append (review.serialize())

        return Response ({"reviews": reviews_content})
