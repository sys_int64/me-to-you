"""

@api {post} /review/write/ Написать отзыв
@apiName WriteReview
@apiGroup Review
@apiDescription

Написать отзыв пользователю с `id` = `to_id`, от пользователя с `id` = `user_id` за выполнение задачи с `id` = `task_id`.

@apiParam {Number} user_id Идентификатор автора отзыва
@apiParam {Number} to_id Идентификатор пользователя, кому отсылается отзыв
@apiParam {Number} task_id Идентификатор задачи, привязанной к отзыву
@apiParam {Number} rank Рейтинг
@apiParam {String} message Отзыв к заданию

@apiSuccess {Boolean} ok `true` если все хорошо
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.db.models               import Q
from review.models                  import Review
from task.models                    import Task

class WriteReviewView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format=None):
        user_id = request.POST.get ("user_id");
        to_id   = request.POST.get ("to_id");
        task_id = request.POST.get ("task_id");
        rank    = request.POST.get ("rank");

        try:
            user = User.objects.get (pk = int(user_id))
            to   = User.objects.get (pk = int(to_id))

        except User.DoesNotExist:
            return Response ({"error": "U0"})

        try:
            task = Task.objects.get (pk = int(task_id))

        except Task.DoesNotExist:
            return Response ({"error": "T0"})

        # Write
        message = request.POST.get ("message")
        Review.objects.create (user = user, to = to, task = task, text = message, rank = int (rank))

        return Response ({"ok": True});
