from django.conf.urls import url
from rest_framework import routers
from django.conf.urls import url, include
from . import views

urlpatterns = [

    url (r'^user/(?P<pid>[0-9]+)/$'                            , views.UserView            .as_view(), name = 'api-user'),
    url (r'^user/my/$'                                         , views.UserMyView          .as_view(), name = 'api-user'),
    url (r'^user/register/$'                                   , views.UserRegisterView    .as_view(), name = 'api-user'),
    url (r'^user/activate/$'                                   , views.UserActivateView    .as_view(), name = 'api-user'),
    url (r'^user/send-code/$'                                  , views.UserSendCodeView    .as_view(), name = 'api-user'),
    url (r'^user/recovery-password/$'                          , views.RecoveryPasswordView.as_view(), name = 'api-user'),
    url (r'^user/update-password/$'                            , views.UpdatePasswordView  .as_view(), name = 'api-user'),
    url (r'^user/update-phone/$'                               , views.UpdatePhoneView2    .as_view(), name = 'api-user'),
    url (r'^user/update-avatar/$'                              , views.UserUpdateAvatar    .as_view(), name = 'api-user'),
    url (r'^user/update-profile/$'                             , views.UserUpdateProfile   .as_view(), name = 'api-user'),

    url (r'^task/(?P<tid>[0-9]+),user_detail/$'                , views.TaskUserView        .as_view(), name = 'api-task'),
    url (r'^task/(?P<tid>[0-9]+)/$'                            , views.TaskView            .as_view(), name = 'api-task'),
    url (r'^tasks/$'                                           , views.TasksAllView        .as_view(), name = 'api-task'),
    url (r'^tasks\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/$'   , views.TasksView           .as_view(), name = 'api-task'),
    url (r'^tasks/my/$'                                        , views.TasksMyAllView      .as_view(), name = 'api-task'),
    url (r'^tasks\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/my/$', views.TasksMyView         .as_view(), name = 'api-task'),

]
