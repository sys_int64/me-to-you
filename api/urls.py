from django.conf.urls import url
from rest_framework import routers
from django.conf.urls import url, include
from .views import CustomTokenView, CustomConvertTokenView

# User

from api.user.details         import UserView, UserMyView, PerfomersView
from api.user.register        import UserRegisterView
from api.user.activate        import UserActivateView
from api.user.send_code       import UserSendCodeView
from api.user.recovery_passwd import RecoveryPasswordView
from api.user.update_passwd   import UpdatePasswordView
from api.user.update_phone    import UpdatePhoneView2
from api.user.update_avatar   import UserUpdateAvatar
from api.user.update_profile  import UserUpdateProfile
from api.user.set_role        import UserSetRole
from api.user.set_gcm_token   import SetGCMTokenView
from api.user.check_new_tasks import CheckNewTasksView

# Notification

from api.notification.details import NotificationsView, NotificationsCountView
from api.notification.dismiss import DissmisNotificationView

# Task

from api.task.details         import *
from api.task.place           import PlaceTaskView
from api.task.update          import UpdateTaskView
from api.task.close           import TaskCloseView
from api.task.cancel          import TaskCancelView
from api.task.search          import SearchTaskView, SearchMyTaskView
from api.task.filter          import FilterTaskView, FilterMyTaskView, FilterTaskAllView
from api.task.migrate_drafts  import MigrateDraftsView
from api.task.show_phone      import ShowTaskPhoneView
from api.task.give_task       import GiveTaskView
from api.task.refuse          import TaskRefuseView
from api.task.complete        import TaskCompleteView

# Review

from api.review.details       import ReviewsView, ReviewsAllView
from api.review.write         import WriteReviewView

urlpatterns = [

    url (r'^user/(?P<pid>[0-9]+)/$'                            , UserView                .as_view(), name = 'api-user'), # Doc
    url (r'^user/my/$'                                         , UserMyView              .as_view(), name = 'api-user'), # Doc
    url (r'^user/register/$'                                   , UserRegisterView        .as_view(), name = 'api-user'), # Doc
    url (r'^user/activate/$'                                   , UserActivateView        .as_view(), name = 'api-user'), # Doc
    url (r'^user/send-code/$'                                  , UserSendCodeView        .as_view(), name = 'api-user'), # Doc
    url (r'^user/recovery-password/$'                          , RecoveryPasswordView    .as_view(), name = 'api-user'), # Doc
    url (r'^user/update-password/$'                            , UpdatePasswordView      .as_view(), name = 'api-user'), # Doc
    url (r'^user/update-phone/$'                               , UpdatePhoneView2        .as_view(), name = 'api-user'), # Doc
    url (r'^user/update-profile/$'                             , UserUpdateProfile       .as_view(), name = 'api-user'), # Doc
    url (r'^user/set-role/$'                                   , UserSetRole             .as_view(), name = 'api-user'), # Doc
    url (r'^user/token/$'                                      , CustomTokenView         .as_view(), name = 'api-user'), # Doc
    url (r'^user/convert-token/$'                              , CustomConvertTokenView  .as_view(), name = 'api-user'), # Doc
    url (r'^user/set-gcm-token/$'                              , SetGCMTokenView         .as_view(), name = 'api-user'), # Doc
    url (r'^user/check-new-tasks/$'                            , CheckNewTasksView       .as_view(), name = 'api-user'), # Doc

    url (r'^task/(?P<tid>[0-9]+),user_detail/$'                , TaskUserView            .as_view(), name = 'api-task'), # Doc
    url (r'^task/(?P<tid>[0-9]+)/$'                            , TaskView                .as_view(), name = 'api-task'), # Doc
    url (r'^tasks/$'                                           , TasksAllView            .as_view(), name = 'api-task'), # Doc
    url (r'^tasks,short/$'                                     , TasksShortView          .as_view(), name = 'api-task'), # Doc
    url (r'^tasks,short,zip/$'                                 , TaskShortZipView        .as_view(), name = 'api-task'), # Doc
    url (r'^tasks\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/$'   , TasksView               .as_view(), name = 'api-task'), # Doc
    url (r'^tasks/my/$'                                        , TasksMyAllView          .as_view(), name = 'api-task'), # Doc
    url (r'^tasks\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/my/$', TasksMyView             .as_view(), name = 'api-task'), # Doc
    url (r'^task/place/$'                                      , PlaceTaskView           .as_view(), name = 'api-task'), # Doc
    url (r'^task/update/(?P<tid>[0-9]+)/$'                     , UpdateTaskView          .as_view(), name = 'api-task'), # Doc
    url (r'^task/close/(?P<tid>[0-9]+)/$'                      , TaskCloseView           .as_view(), name = 'api-task'), # Doc
    url (r'^task/cancel/(?P<tid>[0-9]+)/$'                     , TaskCancelView          .as_view(), name = 'api-task'), # Doc
    url (r'^task/refuse/(?P<tid>[0-9]+)/$'                     , TaskRefuseView          .as_view(), name = 'api-task'), # Doc
    url (r'^task/complete/(?P<tid>[0-9]+)/$'                   , TaskCompleteView        .as_view(), name = 'api-task'), # Doc

    url (r'^tasks/search\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/$'   , SearchTaskView   .as_view(), name = 'api-task'), # Doc
    url (r'^tasks/filter\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/$'   , FilterTaskView   .as_view(), name = 'api-task'), # Doc
    url (r'^tasks/search\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/my/$', SearchMyTaskView .as_view(), name = 'api-task'), # Doc
    url (r'^tasks/filter\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/my/$', FilterMyTaskView .as_view(), name = 'api-task'), # Doc
    url (r'^tasks/filter,short/$'                                     , FilterTaskAllView.as_view(), name = 'api-task'), # Doc
    url (r'^tasks/migrate-drafts/$'                                   , MigrateDraftsView.as_view(), name = 'api-task'),
    url (r'^task/show-phone/(?P<tid>[0-9]+)/$'                        , ShowTaskPhoneView.as_view(), name = 'api-task'), # Doc
    url (r'^task/give-task/(?P<tid>[0-9]+)/$'                         , GiveTaskView     .as_view(), name = 'api-task'), # Doc

    url (r'^notifications/$'                                    , NotificationsView      .as_view(), name = 'api-notification'), # Dic
    url (r'^notifications/count/$'                              , NotificationsCountView .as_view(), name = 'api-notification'), # Doc
    url (r'^notification/dismiss/$'                             , DissmisNotificationView.as_view(), name = 'api-notification'), # Doc

    url (r'^reviews\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/$'  , ReviewsView            .as_view(), name = 'api-reviews'), # Doc
    url (r'^reviews/$'                                          , ReviewsAllView         .as_view(), name = 'api-reviews'), # Doc
    url (r'^review/write/$'                                     , WriteReviewView        .as_view(), name = 'api-reviews'), # Doc

    url (r'^perfomers\[(?P<offset>[0-9]+):(?P<limit>[0-9]+)\]/$', PerfomersView          .as_view(), name = 'api-user'), # Doc

]
