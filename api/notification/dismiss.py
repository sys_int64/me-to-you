"""

@api {post} /notifications/dismiss/ Удалить уведомление
@apiName DismissNotificationCount
@apiGroup Notification
@apiDescription

Удаляет уведомление у пользователя.

@apiParam {Number} id Идентификатор уведомления

@apiSuccess {Number} ok `true` если все хорошо
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from notification.models            import Notification

class DissmisNotificationView (APIView):

    def post (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user = request.user
        nid  = request.POST.get ("id")

        try:
            notification = Notification.objects.get (pk = nid)

        except Notification.DoesNotExist:
             return Response ({"error": "N0"})

        if (user in notification.to.all()):
            #notification.delete()
            notification.exclude.add (user)
            notification.save()
            return Response ({"ok": True})

        return Response ({"error": "N1"})
