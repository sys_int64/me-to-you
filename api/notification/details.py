"""

@api {post} /notifications/ Отображает уведомления пользователя
@apiName Notifications
@apiGroup Notification
@apiDescription

Возвращает массив уведомлений

@apiSuccess {Number}  id Идентификатор уведомления
@apiSuccess {String}  title Заголовок
@apiSuccess {String}  body Содержаение уведомления
@apiSuccess {Boolean} system `true` если уведомление системное
@apiSuccess {String}  datetime Время когда пришло уведомление
@apiSuccess {String}  android_format Формат времени для android \
                        &nbsp;&nbsp;&nbsp;`dd.MM (HH:mm)` - Если уведомление пришло не сегодня \
                        &nbsp;&nbsp;&nbsp;`HH:mm` - Если уведомление пришло сегодня \
@apiSuccess {String}  status Статус уведомления:\
                        &nbsp;&nbsp;&nbsp;<b>0</b> - Не задан\
                        &nbsp;&nbsp;&nbsp;<b>1</b> - Одобрен\
                        &nbsp;&nbsp;&nbsp;<b>2</b> - Отклонен\
                        &nbsp;&nbsp;&nbsp;<b>3</b> - Взять заказ\
                        &nbsp;&nbsp;&nbsp;<b>4</b> - Оценить\
                        &nbsp;&nbsp;&nbsp;<b>5</b> - Новое задание
@apiSuccess {[User]}  user Краткое содержание <a href = "#api-User-GetUser">пользователя</a>, отображается только поля `id`; `fullname`; `avatar`
                        поле `fullname` = `first_name`+" "+`last_name`. Либо `null`, если пользователь не указан.
@apiSuccess {[Task]}  task Краткое содержание <a href = "#api-Task-GetTask">задачи</a>, отображается только поля `id`; `title`; `category`; `my`; `status`; `no_fact_addr`.
                        Либо `null`, если пользователь не указан.

@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from notification.models            import Notification

from django.db.models               import Q
from datetime                       import date
from django.utils                   import formats
from api.task.details               import get_task_short
from django.conf                    import settings
from django.utils import timezone

def get_notify_details (notification):
    user = notification.user
    task = notification.task

    user_details = None
    task_details = None

    if (user):
        avatar_url = "";

        if (user.userprofile.avatar): avatar_url = "{0}{1}".format("http://me2you.pro", user.userprofile.avatar.url)
        else:                         avatar_url = user.userprofile.avatar_url

        user_details = {
            "id"       : user.id,
            "fullname" : user.first_name+" "+user.last_name,
            "avatar"   : avatar_url,
        }

    if (task):
        task_details = get_task_short(task)

    dt_format = "dd.MM (HH:mm)"
    dt = formats.date_format (timezone.localtime(notification.date_time), "d.m (H:i)")

    #if (notification.date_time.date() < date.today()):
    #    dt_format = "dd.MM (HH:mm)"
    #    dt = formats.date_format (notification.date_time, "d.m (H:i)")
    #else:
    #    dt_format = settings.JAVA_TIME_FORMAT
    #    dt = formats.date_format (notification.date_time, "TIME_FORMAT")

    return {
        "id" : notification.id,
        "title" : notification.title,
        "body" : notification.body,
        "status" : int(notification.status),
        "system" : notification.system,
        "datetime" : dt,
        "user" : user_details,
        "task" : task_details,
        "android_format" : dt_format
    }

class NotificationsView (APIView):
	#permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user          = request.user
        notifications = Notification.objects.filter ((
             (Q (to   = user) & Q (to_users = True)) |
            ((Q (role = user.userprofile.utype     ) | Q (role = "0")) & Q (to_users = False)))).order_by("-date_time").all()

        items = []

        for notification in notifications:
            if (not (user in notification.exclude.all())):
                items.append (get_notify_details(notification))

        return Response ({"items": items})

"""

@api {post} /notifications/count/ Количество уведомлений
@apiName NotificationsCount
@apiGroup Notification
@apiDescription

Метод возвращает количество уведомлений пользователя.

@apiSuccess {Number} count Количество уведомлений
@apiError   {String} error Код ошибки

"""

class NotificationsCountView (APIView):

    def get (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user  = request.user
        count = Notification.objects.filter (~Q (exclude = user) & (
             (Q (to   = user) & Q (to_users = True)) |
            ((Q (role = user.userprofile.utype     ) | Q (role = "0")) & Q (to_users = False)))).count()

        return Response ({"count": count})
