"""

@api {post} /user/convert-token/ Преобразовине Access Token
@apiName ConvertAccessToken
@apiGroup AccessToken
@apiDescription

Преобразовывает полученный Access Token с другого сервиса, к примеру с VK, в Access Token \
Me to You. grant_type должен быть равен convert_token\
\
Поля client_id, client_id - обязательные

@apiParam {String} grant_type    Тип запроса:\
                                    &nbsp;&nbsp;&nbsp;<b>convert_token</b> - Преобразование токена от другого сервиса в\
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Access Token сервиса Me to You

@apiParam {String} client_id     Уникальный номер клиента
@apiParam {String} client_secret Секретный ключ клиента
@apiParam {String} backend       Backend сервиса:\
                                    &nbsp;&nbsp;&nbsp;<b>vk-oauth2</b> - Vkontakte\
                                    &nbsp;&nbsp;&nbsp;<b>facebook</b> - Facebook\
                                    &nbsp;&nbsp;&nbsp;<b>google-oauth2</b> - Google+
@apiParam {String} token         Токен, который необходимо преобразовать

@apiSuccess {String} token_type    Тип токена
@apiSuccess {Number} expires_in    Время жизни токена
@apiSuccess {String} refresh_token Токен для обновления Access Token
@apiSuccess {String} access_token  Ключи для авторизации
@apiSuccess {String} scope         Права

@apiSuccessExample Успешный запрос:
    HTTP/1.1 200 OK
    {
        "token_type": "Bearer",
        "expires_in": 36000,
        "refresh_token": "QNTSeIxuR5S0jSuZ5Gyt2jhWN6upRC",
        "access_token": "oBxiT6l8v4HdbmNRe3EwZki5CY7ay2",
        "scope": "write read groups"
    }

"""
