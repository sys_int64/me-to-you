"""

@api {post} /o/revoke_token/ Аннулирование токена
@apiName RevokeAccessToken
@apiGroup AccessToken
@apiDescription

Удаление токена - выход из системы.

@apiParam {String} client_id     Уникальный номер клиента
@apiParam {String} client_secret Секретный ключ клиента
@apiParam {String} token         Токен, который необходимо удалить

@apiSuccess {String} token_type    Тип токена
@apiSuccess {Number} expires_in    Время жизни токена
@apiSuccess {String} refresh_token Токен для обновления Access Token
@apiSuccess {String} access_token  Ключи для авторизации
@apiSuccess {String} scope         Права

"""
