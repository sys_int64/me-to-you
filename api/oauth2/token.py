"""

@api {post} /user/token/ Получение Access Token
@apiName GetAccessToken
@apiGroup AccessToken
@apiDescription

Метод для получение Access Token. Есть три способа получить Access Token:
через ввод пары логин/пароль; через refresh_token для обновление уже имеющегося Access Token,
у которого истек срок жизни; <a href = "#api-AccessToken-ConvertAccessToken">преобразовать Access Token</a> стороннего сервиса (к примеру VK) в
токен приложение.\
\
при <b>grant_type</b> = password необходимо передать username; password;\
при <b>grant_type</b> = refresh_token необходимо передать серверу refresh_token;\
при <b>grant_type</b> = convert_token необходимо передать серверу backend; token\
\
Поля client_id, client_id - обязательные

@apiParam {String} grant_type    Тип запроса:\
                                    &nbsp;&nbsp;&nbsp;<b>password</b> - Получение токена по паре лоигн/пароль,\
                                    &nbsp;&nbsp;&nbsp;<b>refresh_token</b> - Обновление Access Token,\
                                    &nbsp;&nbsp;&nbsp;<b>convert_token</b> - Преобразование токена от другого сервиса в\
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Access Token сервиса Me to You

@apiParam {String} client_id     Уникальный номер клиента
@apiParam {String} client_secret Секретный ключ клиента
@apiParam {String} username      Имя пользователя
@apiParam {String} password      Пароль
@apiParam {String} refresh_token Токен для обновления существующего токена

@apiSuccess {String} token_type    Тип токена
@apiSuccess {Number} expires_in    Время жизни токена
@apiSuccess {String} refresh_token Токен для обновления Access Token
@apiSuccess {String} access_token  Ключи для авторизации
@apiSuccess {String} scope         Права

@apiSuccessExample Успешный запрос:
    HTTP/1.1 200 OK
    {
        "token_type": "Bearer",
        "expires_in": 36000,
        "refresh_token": "QNTSeIxuR5S0jSuZ5Gyt2jhWN6upRC",
        "access_token": "oBxiT6l8v4HdbmNRe3EwZki5CY7ay2",
        "scope": "write read groups"
    }

"""
