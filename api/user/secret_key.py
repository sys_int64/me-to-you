import random
import string

from datetime     import timedelta
from django.utils import timezone

def id_generator (size = 6, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def key_valid (enter_key, user):
    valid_key     = user.userprofile.secret
    enter_key     = enter_key.upper()

    return (enter_key == valid_key and valid_key.strip() != "")

def key_expired (user):
    secret_change = user.userprofile.secret_change
    return not (secret_change+timedelta (minutes = 50) > timezone.now())

def clear_key (user):
    user.userprofile.secret = ""
    user.userprofile.secret_change = timezone.now()
    user.userprofile.save()
