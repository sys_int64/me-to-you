"""

@api {get} /api/user/set_role/ Задать роль пользователю
@apiName SetRole
@apiGroup User
@apiDescription
Задает новую роль пользователю.

@apiParam {String} role Роль:\
                           &nbsp;&nbsp;&nbsp;<b>1</b> - Заказчик\
                           &nbsp;&nbsp;&nbsp;<b>2</b> - Исполнитель

@apiSuccess {String} ok     True, если ошибок нет
@apiError   {String} error  Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView
from django.contrib.auth.models     import User

class UserSetRole (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format=None):
        if (not request.user.id):
            return Response ({"error" : "U0"})

        user = request.user

        if (user.userprofile.utype != "0"):
            return Response ({"error" : "U9"})

        val = str(request.POST.get ("role"))
        print (val)

        if (val != "1" and val != "2"):
            return Response ({"error" : "U10"})

        user.userprofile.utype = val
        user.userprofile.save()

        return Response ({"ok" : True})
