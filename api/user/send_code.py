"""

@api {post} /user/send-code/ Отправка кода подтверждения
@apiName SendCodeUser
@apiGroup User
@apiDescription

Отправка кода с подтверждением на e-mail пользователя.\
\
при восстановлении пароля `action` должно быть равным `recovery-password`,
необходимо заполнить параметры `phone` номер телефона, который привязан к
пользователю; `password` - новый пароль.\
\
Для данного метода обязательно нужна авторизация, за исключением восстановиления
пароля.

@apiParam {String} client_id     Уникальный номер клиента
@apiParam {String} client_secret Секретный ключ клиента
@apiParam {String} action        Действие
@apiParam {String} password      Новый пароль, при action = `recovery-password`

@apiSuccess {String} ok          True, если ошибок нет
@apiError   {String} error       Код ошибки

@apiSuccessExample Успешный запрос:
    HTTP/1.1 200 OK
    {
        "ok": True,
    }

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView
from django.contrib.auth.models     import User

import datetime

from datetime import timedelta
from django.utils import timezone

from django.core.mail import send_mail
#import api.user.secret_key
from api.user.secret_key import id_generator

class UserSendCodeView (APIView):
    permission_classes = (AllowAny,) # TODO!!!!

    def post (self, request, format = None):

        #send_mail ('Проверочный код Me2You', 'Код: Test', 'robot@me2you.pro', ['andrey@kabylin.ru'])
        #return Response ({"ok": True})

        if (not request.user.id):
            try:
                user = User.objects.get (email = request.POST.get ("email"))

            except User.DoesNotExist:
                return Response ({"error" : "U0"})

        else:
            user = request.user

        key    = id_generator()
        c_type = request.POST.get ("action")

        if (c_type == "update-phone"):
            phone = request.POST.get ("phone")
            user.userprofile.accept_phone = False
            user.userprofile.new_phone    = phone

        elif (c_type == "recovery-password"):
            passwd = request.POST.get ("password")
            user.userprofile.accept_passwd = False
            user.userprofile.new_passwd    = passwd

        user.userprofile.secret        = key
        user.userprofile.secret_change = timezone.now()
        user.userprofile.save()

        if (user.userprofile.social):
            return Response ({"error": "U11"})

        send_mail ('Проверочный код Me2You', 'Код: '+key, 'robot@me2you.pro', [user.email])

        return Response ({"ok": True})

        result = send_sms (user.userprofile.phone, key, "smstest")
        result_json = result.json()
        result_e = ""

        try:
            for each in result_json[0]:
                result_e = each

            sms_error = result_json[0][result_e]["error"];

            if (sms_error == "0"):
                return Response ({"ok": True})
            else:
                return Response ({"error": "U6", "sms_error": sms_error+":"+str(result.status_code)})

        except:
            return Response ({"error": "U6", "sms_error": "accaunt_deny"+":"+str(result.status_code)})
