"""

@api {post} /user/register/ Регистрация пользователя
@apiName RegisterUser
@apiGroup User
@apiDescription

Регистрация пользователя. После регистрации автоматически отправляется проверочный
код, для подтверждения телефона.

@apiParam {String} client_id     Уникальный номер клиента
@apiParam {String} client_secret Секретный ключ клиента
@apiParam {String} phone         Номер телефона пользователя
@apiParam {String} password      Пароль
@apiParam {String} email         E-mail пользователя, не обязательное поле
@apiParam {String} avatar        Картинка в формате base64

@apiSuccess {String} ok          True, если все регистрация пошла успешно
@apiError   {String} error       Код ошибки

@apiSuccessExample Успешный запрос:
    HTTP/1.1 200 OK
    {
        "ok": True,
    }

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

#from django.conf                    import settings
from django.contrib.auth.models     import User
#from base64                         import b64decode

import requests
import string
import random
import hashlib
#import os
import api.user.secret_key

from time import time
from django.core.files.base import ContentFile
import os
from base64 import b64decode
from django.conf import settings
from userprofile.models import UserProfile

# RED SMS API

def redsms_signature (params, api_key):
    params = sorted (params)
    params_s = ""

    for sub in params: params_s += str(sub[1])

    params_s    += api_key
    params_hash  = hashlib.md5 (params_s.encode('utf-8')).hexdigest()

    return params_hash

def red_sms_api (method, params):
    signature       = redsms_signature (params, "620052c2ebff890561a0eb59959554cf387ee09a") # TODO: Move to Server Enviroment
    red_sms_request = "https://lk.redsms.ru/get/"+method+".php?signature="+signature

    for param in params:
        red_sms_request += "&"+str(param[0])+"="+str(param[1])

    return requests.get (red_sms_request)

def send_sms (to, message, sender):
    timestamp_r = requests.get ('https://lk.redsms.ru/get/timestamp.php')
    timestamp   = timestamp_r.json()

    login = "sys.int64" # TODO: Move to Server Enviroment
    phone = to

    params = [
        ("timestamp", timestamp),
        ("login", login),
        ("phone", phone),
        ("text" , message),
        ("sender", sender),
        ("return", "json"),
    ]

    return red_sms_api ("send", params)

#def id_generator (size = 6, chars=string.ascii_uppercase + string.digits):


def uploadAvatar (base64_string, user):

    decoded_image = b64decode (base64_string)
    filename = "uploaded_image%s.jpg" % str(time()).replace('.','_')

    fh = open (os.path.join (settings.MEDIA_ROOT, filename), "wb")
    fh.write  (decoded_image)
    fh.close()

    user.userprofile.avatar_url = ""
    user.userprofile.avatar     = ContentFile (decoded_image, filename)
    user.userprofile.save()

    return user.userprofile.avatar.url

def random_username(length=30, chars=string.ascii_lowercase+string.digits):
    username = ''.join(random.choice(chars) for _ in range(length))
    try:
        User.objects.get(username=username)
        return random_username(length=length, chars=chars)
    except User.DoesNotExist:
        return username;

class UserRegisterView (APIView):
    permission_classes = (AllowAny,)

    def post (self, request, format=None):

        phone = request.POST.get("phone")

        if (phone != None and phone.strip() != ""):
            try:
                UserProfile.objects.get (phone = phone)
                return Response ({"error" : "U5"})
            except UserProfile.DoesNotExist:
                pass

        try:
            User.objects.get (email = request.POST.get("email"))
            return Response ({"error" : "U14"})
        except User.DoesNotExist:
            pass

        user = User.objects.create_user (random_username(), request.POST.get("email"), request.POST.get("password"))
        #key  = id_generator()

        #user.userprofile.secret = key
        if (phone != None and phone.strip() != ""):
            user.userprofile.phone = request.POST.get ("phone")

        base64 = request.POST.get ("avatar")

        if (base64 != None and base64.strip() != ""):
            uploadAvatar (base64, user)
        else:
            user.userprofile.avatar_url = "http://me2you.pro/media/no_avatar.jpg"

        user.userprofile.save()
        user.save()

        return Response ({"ok": "1"})

        result = send_sms (user.userprofile.phone, key, "smstest")
        result_json = result.json()
        result_e = ""

        try:
            for each in result_json[0]:
                result_e = each

            sms_error = result_json[0][result_e]["error"];

            if (sms_error == "0"):
                return Response ({"ok": True})
            else:
                user.delete()
                return Response ({"error": "U6", "sms_error": sms_error+":"+str(result.status_code)})

        except:
            user.delete()
            return Response ({"error": "U6", "sms_error": "accaunt_deny"+":"+str(result.status_code)})
