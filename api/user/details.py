"""

@api {get} /user/:id/ Инфомация о пользователе
@apiName GetUser
@apiGroup User
@apiDescription

Получение информации о пользователе по уникальному номеру, либо, если пользователь
авторизован, можно получиться данные выполнив запрос `/user/my/` для получения
данных об авторизованном пользователе.

@apiParam {Number} id Уникальный номер пользователя.

@apiSuccess {Number}  id               Уникальный номер пользователя
@apiSuccess {Number}  email            Уникальный номер пользователя
@apiSuccess {String}  firstname        Имя
@apiSuccess {String}  lastname         Фамилия
@apiSuccess {String}  phone            Телефон
@apiSuccess {Boolean} active           True, если пользователь активирован
@apiSuccess {String}  skills           Навыки исполнителя
@apiSuccess {String}  gender           Пол пользователя: 0 - не задан; m - мужской; а - женский
@apiSuccess {String}  birthdate        Дата рождение. Формат даты: d.m.Y
@apiSuccess {String}  city             Город
@apiSuccess {String}  social           `true`, если пользователь зашел через соц. сеть
@apiSuccess {String}  avatar           URL аватара пользователя
@apiSuccess {Number}  type             Тип пользователя:\
                                            &nbsp;&nbsp;&nbsp;<b>0</b> - Не задан\
                                            &nbsp;&nbsp;&nbsp;<b>1</b> - Заказчик\
                                            &nbsp;&nbsp;&nbsp;<b>2</b> - Исполнитель
@apiSuccess {Number}  rank             Рейтинг пользователя
@apiSuccess {Number}  tasks_count      Количество выполненных заданий для Исполнителя, и количество размещенных заданий для Заказчика
@apiSuccess {String}  date_joined      Дата регистрации на сервисе
@apiSuccess {Number}  my_reviews       Количество моих отзывов
@apiSuccess {Number}  about_me_reviews Количество отзывов обо мне
@apiSuccess {Array}   subscription     Массив подписанных категорий:\
                                            &nbsp;&nbsp;&nbsp;<b>0</b> - Бытовой ремонт\
                                            &nbsp;&nbsp;&nbsp;<b>1</b> - Курьерские услуги\
                                            &nbsp;&nbsp;&nbsp;<b>2</b> - Уборка и помощь по хозяйству\
                                            &nbsp;&nbsp;&nbsp;<b>3</b> - Грузоперевозки\
                                            &nbsp;&nbsp;&nbsp;<b>4</b> - Компьютерная помощь\
                                            &nbsp;&nbsp;&nbsp;<b>5</b> - Мероприятия и промо-акции\
                                            &nbsp;&nbsp;&nbsp;<b>6</b> - Удаленная работа\
                                            &nbsp;&nbsp;&nbsp;<b>7</b> - Дизайн\
                                            &nbsp;&nbsp;&nbsp;<b>8</b> - Web-разработка\
                                            &nbsp;&nbsp;&nbsp;<b>9</b> - Фото- и видео-услуги\
                                            &nbsp;&nbsp;&nbsp;<b>10</b> - Ремонт цифровой техники\
                                            &nbsp;&nbsp;&nbsp;<b>11</b> - Установка и ремонт техники\
                                            &nbsp;&nbsp;&nbsp;<b>12</b> - Красота и здоровье\
                                            &nbsp;&nbsp;&nbsp;<b>13</b> - Юридическая помощь

@apiSuccessExample Успешный запрос:
    HTTP/1.1 200 OK
    {
        "my_reviews": 0,
        "rank": 0,
        "birthdate": "",
        "phone": "79137698347",
        "gender": "m",
        "active": true,
        "tasks_count": 0,
        "type": 2,
        "id": 2,
        "about_me_reviews": 0,
        "lastname": "Кабылин",
        "email": "",
        "city": "Новосибирск",
        "firstname": "Андрей",
        "skills": "C/C++; LLVM; ASM;\nPython; Django.",
        "avatar": "http://me2you.pro/media/uploaded_image1455436515_283432_SDVKdAJ.jpg",
        "subscription": [
            8,
            5
        ],
        "date_joined": "декабря 2015"
    }

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.utils                   import formats
from task.models                    import Task
from review.models                  import Review
from userprofile.models             import UserProfile
from django.db.models               import Q
from sorl.thumbnail import get_thumbnail

def get_profile (user):
    avatar_url = "";

    if (user.userprofile.avatar):
        avatar_im = get_thumbnail(user.userprofile.avatar, '200x200', crop='center', quality=99)
        avatar_url = "{0}{1}".format("http://bizzi.pro", avatar_im.url)
    else:
        avatar_url = user.userprofile.avatar_url

    monthes = ["января", "февраля", "марта"   , "апреля" , "мая"   , "июня",
               "июля"  , "августа", "сентября", "октября", "ноября", "декабря"]

    user.userprofile.update_rank()
    user.userprofile.update_task_count()
    user.userprofile.update_task_count2()

    my_reviews       = Review.objects.filter (approved = True, user = user).count()
    about_me_reviews = Review.objects.filter (approved = True, to   = user).count()

    if (str(user.userprofile.birthdate) == "1800-01-01"):
        birthdate = ""
    else:
        birthdate = formats.date_format (user.userprofile.birthdate, "DATE_FORMAT")

    cats = []
    cats_set = user.category_set.all()

    for cat in cats_set:
        cats.append (int(cat.category))

    return {
        'id'               : user.id,
        'email'            : user.email,
        'firstname'        : user.first_name,
        'lastname'         : user.last_name,
        'avatar'           : avatar_url,
        'gender'           : user.userprofile.gender,
        'city'             : user.userprofile.city,
        'skills'           : user.userprofile.skills,
        'birthdate'        : birthdate,
        'active'           : user.userprofile.active,
        'phone'            : user.userprofile.phone,
        'type'             : int(user.userprofile.utype),
        'rank'             : user.userprofile.rank,
        'start'            : 0,
        'tasks_count'      : user.userprofile.tasks_count,
        'tasks_count2'     : user.userprofile.tasks_count2,
        'date_joined'      : monthes[user.date_joined.month-1]+" "+str(user.date_joined.year),
        'my_reviews'       : my_reviews,
        'about_me_reviews' : about_me_reviews,
        'subscription'     : cats,
        'social'           : user.userprofile.social,
    }

def get_perfomer (user):
    avatar_url = "";

    if (user.userprofile.avatar):
        avatar_im = get_thumbnail(user.userprofile.avatar, '200x200', crop='center', quality=99)
        avatar_url = "{0}{1}".format("http://bizzi.pro", avatar_im.url)
    else:
        avatar_url = user.userprofile.avatar_url

    user.userprofile.update_rank()
    user.userprofile.update_task_count()

    return {
        'id'          : user.id,
        'firstname'   : user.first_name,
        'lastname'    : user.last_name,
        'avatar'      : avatar_url,
        'city'        : user.userprofile.city,
        'rank'        : user.userprofile.rank,
        'tasks_count' : user.userprofile.tasks_count,
    }

class UserView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, pid, format=None):

        content = {}

        try:
            user = User.objects.get (pk = pid)
            content = get_profile (user)

        except User.DoesNotExist:
            content = {"error": "U0"}

        return Response (content)

class UserMyView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format=None):

        if (request.user.id):
            user = request.user
            content = get_profile (user)

        else:
            content = {"error": "U0"}

        return Response (content)

"""

@api {get} /perfomers[:offset,:limit]/ Список исполнителей
@apiName GetPerfomers
@apiGroup User
@apiDescription

Отображает список исполнителей в диапазоне [`:offset`,`:limit`]

@apiParam {Number} offset Количество пропущенных исполнителей
@apiParam {Number} limit  Максимальное количество исполнителей

@apiSuccess {Object} perfomers   Исполнители
@apiSuccess {Number} id          Уникальный номер исполнителя
@apiSuccess {String} firstname   Имя
@apiSuccess {String} lastname    Фамилия
@apiSuccess {Number} rank        Рейтинг исполнителя
@apiSuccess {String} city        Город
@apiSuccess {Number} tasks_count Количество выполненных заданий

@apiSuccessExample Успешный запрос:
    HTTP/1.1 200 OK
    {
        "perfomers": [
            {
                "lastname": "Белов",
                "rank": 5,
                "city": "Новосибирск",
                "id": 16,
                "avatar": "http://me2you.pro/media/uploaded_image1451978125_4634757_1VmcaYO.jpg",
                "tasks_count": 1,
                "firstname": "Александр"
            },
            {
                "lastname": "Зуев",
                "rank": 5,
                "city": "Новосибирск",
                "id": 24,
                "avatar": "http://me2you.pro/media/uploaded_image1455352754_7142942_n7On8vV.jpg",
                "tasks_count": 1,
                "firstname": "Алексей"
            }
        ]
    }
@apiError {String} error Код ошибки

"""

class PerfomersView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):

        order_by = request.GET.get ("sort")
        search   = request.GET.get ("search")

        if   (order_by == "0"): order_by   = "-rank"
        elif (order_by == "1"): order_by   = "-tasks_count"
        else: order_by = "-rank"

        profiles = UserProfile.objects.filter(~Q(user__first_name="") &
                        ~Q(user__last_name="") &
                        Q(utype="2") & (Q(user__first_name__icontains=search) |
                        Q(skills__icontains = search))).order_by(order_by)[int(offset):int(offset)+int(limit)]

        #profiles = profiles[int(offset):(int(offset)+int(limit))]
        perfomers = []

        for profile in profiles:
            profile.update_task_count()
            perfomers.append (get_perfomer (profile.user))

        return Response ({"perfomers":perfomers})
        #try:
        #    user = User.objects.get (pk = pid)
        #    content = get_profile (user)

        #except User.DoesNotExist:
        #    content = {"error": "U0"}

        #return Response (content)
