"""

@api {post} /user/update-profile/ Обновление профильных данных
@apiName UpdateProfileUser
@apiGroup User

@apiParam {String} firstname  Имя
@apiParam {String} lastname   Фамилия
@apiParam {String} gender     Пол пользователя:\
                                &nbsp;&nbsp;&nbsp;<b>0</b> - не задан;\
                                &nbsp;&nbsp;&nbsp;<b>m</b> - мужской;\
                                &nbsp;&nbsp;&nbsp;<b>а</b> - женский
@apiParam {String} city       Город
@apiParam {String} skills     Навыки исполнителя
@apiParam {String} birthdate  Дата рождения. Формат даты: d.m.Y
@apiParam {String} avatar     Изображение в base64

@apiSuccess {String} ok    True, если ошибок нет
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from time import time
from django.core.files.base import ContentFile
import os
from base64 import b64decode
from django.conf import settings

def uploadAvatar (base64_string, user):

    decoded_image = b64decode (base64_string)
    filename = "uploaded_image%s.jpg" % str(time()).replace('.','_')

    fh = open (os.path.join (settings.MEDIA_ROOT, filename), "wb")
    fh.write  (decoded_image)
    fh.close()

    user.userprofile.avatar_url = ""
    user.userprofile.avatar     = ContentFile (decoded_image, filename)
    user.userprofile.save()

class UserUpdateProfile (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user    = request.user
        profile = user.userprofile

        if request.POST.get("birthdate") is not None:
            if (request.POST.get ("birthdate").strip() == ""):
                birthDate = "1800-01-01"
            else:

                try:
                    birthDate = request.POST.get ("birthdate").split (".")
                    birthDate = birthDate[2]+"-"+birthDate[1]+"-"+birthDate[0]
                except:
                    return Response ({"error" : "U15"})

        if request.POST.get("firstname") is not None:
            user.first_name   = request.POST.get ("firstname")

        if request.POST.get("lastname") is not None:
            user.last_name    = request.POST.get ("lastname")

        if request.POST.get("gender") is not None:
            profile.gender    = request.POST.get ("gender")

        if request.POST.get("city") is not None:
            profile.city      = request.POST.get ("city")

        if request.POST.get("skills") is not None:
            profile.skills    = request.POST.get ("skills")

        if request.POST.get("birthdate") is not None:
            profile.birthdate = birthDate

        perform = request.POST.get ("perform")

        if (perform is not None):
            if (perform == "1"):
                profile.utype = "2"
            else:
                profile.utype = "1"

        base64 = request.POST.get ("avatar")

        if (base64 is not None and base64.strip() != ""):
            uploadAvatar (base64, user)

        profile.save()
        user   .save();

        return Response ({"ok" : True})
