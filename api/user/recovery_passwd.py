"""

@api {post} /user/recovery-password/ Восстановление пароля
@apiName RecoveryPasswordUser
@apiGroup User
@apiDescription

Для восстановления пароля через `email`, для начала необходимо получить защитный ключи с помощью
метода <a href = "#api-User-SendCodeUser">/user/send-code/</a>, с указанием
e-mail и нового пароля, после чего на новый номер отправляется защитный ключ,
если пользователь вводит верно, то у пользователя меняется пароль, иначе необходимо
снова получить защитный ключ.\
Если восстановление пароля происходит через Digits,
то необходимо задать параметры `phone`, `X-Auth-Service-Provider`, `X-Verify-Credentials-Authorization`
для проверки правильности ввода ключа.\
Подробнее: <a href = "https://dev.twitter.com/oauth/echo" target = "_blank">https://dev.twitter.com/oauth/echo</a>

@apiParam {String} key           Проверочный ключ
@apiParam {String} phone         Телефон
@apiParam {String} email         E-mail
@apiParam {String} X-Auth-Service-Provider Хост куда отправить
@apiParam {String} X-Verify-Credentials-Authorization OAuth токены, и сигнатуры для Digits

@apiSuccess {String} ok          True, если ошибок нет
@apiError   {String} error       Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView
from django.contrib.auth.models     import User

from api.user.secret_key import *

class RecoveryPasswordView (APIView):
    permission_classes = (AllowAny,) # TODO!!!!

    def post (self, request, format = None):

        try:
            print (request.POST.get ("phone"))
            user = User.objects.get (email = request.POST.get ("email"))

        except User.DoesNotExist:
            return Response ({"error" : "U0"})

        key = request.POST.get ("key")
        #user = request.user

        if (key_expired (user)):
            clear_key (user)
            return Response ({"error" : "U2"})

        if (not key_valid (key, user)):
            clear_key (user)
            return Response ({"error" : "U3"})

        clear_key (user)
        user.set_password (user.userprofile.new_passwd)

        user.userprofile.accept_passwd = True
        user.userprofile.new_passwd = ""
        user.userprofile.save()
        user.save()

        return Response ({"ok" : True})
