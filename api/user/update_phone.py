"""

@api {post} /user/update-phone/ Обновление телефона
@apiName UpdatePhoneUser
@apiGroup User
@apiDescription

Обновление телефона проиходит через сервис <a href = "https://get.digits.com/" target = "_blank">Digits</a>.
На стороне сервера проверяется правильность ввода проверочного кода через токены и сигнатуры Digits, которые
хранятся в поле `X-Verify-Credentials-Authorization`, подробнее:
<a href = "https://docs.fabric.io/android/digits/verify-user.html#generating-oauth-echo-headers">Android</a>\;
<a href = "https://docs.fabric.io/ios/digits/oauth-echo.html#obtaining-oauth-echo-headers">iOS</a>.

@apiParam {String} key           Проверочный ключ
@apiParam {String} X-Auth-Service-Provider Хост куда отправить
@apiParam {String} X-Verify-Credentials-Authorization OAuth токены, и сигнатуры для Digits

@apiSuccess {String} ok          True, если ошибок нет
@apiError   {String} error       Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView
from userprofile.models             import UserProfile

import requests

class UpdatePhoneView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        key       = request.POST.get ("key")
        user      = request.user
        phone     = user.userprofile.new_phone

        try:
            User.objects.get (username = phone)
            return Response ({"error" : "U5"})
        except User.DoesNotExist:
            pass

        if (key_expired (user)):
            clear_key (user)
            return Response ({"error" : "U2"})

        if (not key_valid (key, user)):
            clear_key (user)
            return Response ({"error" : "U3"})

        clear_key (user)
        user.userprofile.accept_phone = True
        user.userprofile.phone        = phone
        user.userprofile.save()

        if (user.has_usable_password()):
            user.username = phone

        user.save()

        return Response ({"ok" : True})

from urllib.parse import urlparse

class UpdatePhoneView2 (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    #permission_classes = (AllowAny,)

    def post (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        try:
            UserProfile.objects.get (phone = request.POST.get("phone"))
            return Response ({"error" : "U5"})
        except UserProfile.DoesNotExist:
            pass

        user        = request.user
        request_url = request.POST.get ("X-Auth-Service-Provider")
        hostname    = urlparse (request_url).hostname

        if (hostname != "api.digits.com" and hostname != "api.twitter.com"):
            return Response ({"error": "U6"})

        headers = {
            "Authorization": request.POST.get ("X-Verify-Credentials-Authorization"),
        }

        verify_credentials      = requests.get (request_url, headers=headers)
        verify_credentials_json = verify_credentials.json()

        try:
            if (verify_credentials_json.get("errors")):
                return Response ({"error": "U7", "digits_verify_credentials": verify_credentials_json})

            if (verify_credentials_json.get("verification_type") == "sms"):

                user.userprofile.phone  = request.POST.get ("phone")
                user.userprofile.active = True
                user.userprofile.save()

                return Response ({"ok" : True})
            else:
                return Response ({"error": "U7", "digits_verify_credentials": verify_credentials_json})

        except:
            return Response ({"error": "U7", "digits_verify_credentials": verify_credentials_json})

        return Response ({"error" : "U8"})
