

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

class UserActivateView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user = request.user

        if (user.userprofile.active):
            return Response ({"error" : "U1"})

        secret_change = user.userprofile.secret_change
        valid_key     = user.userprofile.secret
        enter_key     = request.POST.get("key").upper()

        if (secret_change+timedelta (minutes = 50) > timezone.now()):

            if (enter_key == valid_key and valid_key.strip() != ""):
                user.userprofile.secret = ""
                user.userprofile.secret_change = timezone.now()
                user.userprofile.active = True
                user.userprofile.save()

                return Response ({"ok": True})

            else:
                user.userprofile.secret = ""
                user.userprofile.secret_change = timezone.now()
                user.userprofile.save()

                return Response ({"error" : "U3"})

        else:
            user.userprofile.secret = ""
            user.userprofile.secret_change = timezone.now()
            user.userprofile.save()

            return Response ({"error" : "U2"})
