"""

@api {post} /user/set-gcm-token/ Сохранение GCM токена
@apiName SetGCMTokenUser
@apiGroup User
@apiDescription

Сохраняет GCM токен в базе данныз для авторизованного пользователя

@apiParam {String} token         Значение токена

@apiSuccess {String} ok          True, если ошибок нет
@apiError   {String} error       Код ошибки

@apiSuccessExample Успешный запрос:
    HTTP/1.1 200 OK
    {
        "ok": True,
    }

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.conf                    import settings
from django.contrib.auth.models     import User

class SetGCMTokenView (APIView):

    def post (self, request, format=None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user = request.user
        user.userprofile.gcm_token = request.POST.get ("token")
        user.userprofile.save()

        return Response ({"ok" : True})
