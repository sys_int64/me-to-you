"""

@api {post} /user/update-avatar/ Обновление аватара
@apiName UpdateAvatarUser
@apiGroup User
@apiDescription

Для обновления аватара заполняется параметр base64 - данные картинки в кодировке
base64.

@apiParam {String} base64  Изображение

@apiSuccess {String} ok    True, если ошибок нет
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

class UserUpdateAvatar (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user          = request.user
        base64_string = request.POST.get ("base64")
        decoded_image = b64decode (base64_string)

        filename = "uploaded_image%s.jpg" % str(time()).replace('.','_')

        fh = open (os.path.join (settings.MEDIA_ROOT, filename), "wb")
        fh.write  (decoded_image)
        fh.close()

        user.userprofile.avatar_url = ""
        user.userprofile.avatar     = ContentFile (decoded_image, filename)
        user.userprofile.save()

        return Response ({"url" : "{0}{1}".format("http://e2dit.ru", user.userprofile.avatar.url)})
