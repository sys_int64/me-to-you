"""

@api {post} /user/check-new-tasks/ Проверка новых заданий
@apiName CheckNewTasksUser
@apiGroup User
@apiDescription

Посылает запрос серверу на проверку новых заданий, если задания есть, то сервером
отправляется уведомление пользователю о новых заданиях
ключ с помощью метода <a href = "#api-User-SendCodeUser">/user/send-code/</a>.

@apiSuccess {String} ok          True, если ошибок нет
@apiError   {String} error       Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from task.models                    import Task
from notification.models            import Notification
from django.db.models               import Q

from math import sin, cos, asin, acos

class CheckNewTasksView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format=None):

        if (not request.user.id):
            return Response ({"error": "U0"})

        user = request.user
        categories = user.category_set.all()
        query = None

        for cat in categories:
            if (not query): query =         Q (category = cat.category)
            else:           query = query | Q (category = cat.category)

        if (not query):
            return Response ({"ok": True})

        query = query & (~Q(user = user))
        tasks = Task.objects.filter(query).all()

        for task in tasks:

            if (task in user.userprofile.old_tasks.all()):
                continue

            if (task.user == user):
                continue

            user.userprofile.old_tasks.add (task)
            Notification.generate_new_task (task, user)

        #max_dist = 10
        #ignore_cats = ['6', '7', '8']
        #approved_tasks = []
        #R = 6371 # Радиус земли в км

        #lat1 = float(request.GET.get ("lat"))
        #lng1 = float(request.GET.get ("lng"))

        #for task in tasks:
        #    if task.category in ignore_cats:
        #        approved_tasks.append(task.serialize())
        #        continue

        #    lat2 = task.location[1]
        #    lng2 = task.location[0]

        #    dist = acos( sin(lat1)*sin(lat2)+cos(lat1)*cos(lat2)*cos(lng1-lng2) )*R

        #    if (dist <= max_dist):
        #        approved_tasks.append(task.serialize())

        return Response ({"ok": True})
