"""

@api {post} /user/update-password/ Обновление пароля
@apiName UpdatePasswordUser
@apiGroup User
@apiDescription

Для восстановления пароля, для начала необходимо получить защитный ключи с помощью
метода <a href = "#api-User-SendCodeUser">/user/send-code/</a>, с указанием
номера телефона и нового пароля, после чего на новый номер отправляется защитный ключ,
если пользователь вводит верно, то у пользователя меняется пароль, иначе необходимо
снова получить защитный ключ.

@apiParam {String} old-password  Действующий пароль
@apiParam {String} new-password  Новый пароль

@apiSuccess {String} ok          True, если ошибок нет
@apiError   {String} error       Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

class UpdatePasswordView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format = None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user = request.user

        old_password = request.POST.get ("old-password")
        new_password = request.POST.get ("new-password")

        if (not user.check_password (old_password)):
            return Response ({"error" : "U4"})

        user.set_password (new_password)
        user.save()

        return Response ({"ok" : True})
