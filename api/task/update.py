"""

@api {post} /task/update/:id/ Обновить задание
@apiName UpdateTask
@apiGroup Task
@apiDescription

Обновление задачи, после обновления задача попадает либо в раздел модерации, либо в черновик, если `draft` = `true`

@apiParam {Boolean} draft Если `true` помещает задание в черновик
@apiParam {String}  title Заголовок задачи
@apiParam {Number}  category Категория, в которой разместить задание:\
                        &nbsp;&nbsp;&nbsp;<b>0</b> - Бытовой ремонт\
                        &nbsp;&nbsp;&nbsp;<b>1</b> - Курьерские услуги\
                        &nbsp;&nbsp;&nbsp;<b>2</b> - Уборка и помощь по хозяйству\
                        &nbsp;&nbsp;&nbsp;<b>3</b> - Грузоперевозки\
                        &nbsp;&nbsp;&nbsp;<b>4</b> - Компьютерная помощь\
                        &nbsp;&nbsp;&nbsp;<b>5</b> - Мероприятия и промо-акции\
                        &nbsp;&nbsp;&nbsp;<b>6</b> - Удаленная работа\
                        &nbsp;&nbsp;&nbsp;<b>7</b> - Дизайн\
                        &nbsp;&nbsp;&nbsp;<b>8</b> - Web-разработка\
                        &nbsp;&nbsp;&nbsp;<b>9</b> - Фото- и видео-услуги\
                        &nbsp;&nbsp;&nbsp;<b>10</b> - Ремонт цифровой техники\
                        &nbsp;&nbsp;&nbsp;<b>11</b> - Установка и ремонт техники\
                        &nbsp;&nbsp;&nbsp;<b>12</b> - Красота и здоровье\
                        &nbsp;&nbsp;&nbsp;<b>13</b> - Юридическая помощь
@apiParam {String}  desc Описание задачи
@apiParam {Number}  price Цена
@apiParam {String}  address Адрес задачи
@apiParam {Number}  lat Широта, по которому распологается маркер
@apiParam {Number}  lng Долгота, по которому распологается маркер
@apiParam {String}  date_time Когда необходимо выполнить, формат: `dd.mm.YYYY H:i`
@apiParam {String}  life_time Время жизни задачи, формат: `dd.mm.YYYY H:i`
@apiParam {String[:n]}  photo Фотография в формате base64 с порядковым номером `:n`, т.е. `photo[0]` - самая первая фотография

@apiSuccess {String} ok    True, если ошибок нет
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from task.models import Task, Photo
from django.contrib.gis.geos import Point
from time import time
from django.core.files.base import ContentFile, File
from django.core.files.temp import NamedTemporaryFile
import os
from base64 import b64decode
from django.conf import settings
import urllib.request
import requests

def formatDate (date):
    pass

def formatDateTime (datetime):
    if (datetime is None or datetime.strip() == ""):
        return "1800-01-01 00:00:00"
    else:
        parts = datetime.split (" ")
        date  = parts[0].split (".")
        time  = parts[1].split (":")

        return date[2]+"-"+date[1]+"-"+date[0]+" "+time[0]+":"+time[1]+":00"

def upload_photo (base64_string):

    decoded_image = b64decode (base64_string)
    filename = "uploaded_image%s.jpg" % str(time()).replace('.','_')

    fh = open (os.path.join (settings.MEDIA_ROOT, filename), "wb")
    fh.write  (decoded_image)
    fh.close()

    return ContentFile (decoded_image, filename)

def upload_photo_url (image_url):

    filename = "uploaded_image%s.jpg" % str(time()).replace('.','_')
    return ContentFile (requests.get(image_url).content, filename)

class UpdateTaskView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, tid, format=None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        try:
            task = Task.objects.get (pk = tid)

        except Task.DoesNotExist:
            content = {"error": "T0"}

        user = request.user

        if (user != task.user):
            return Response ({"error": "T?"}) # TODO: Set Error Number

        draft  = request.POST.get ("draft")
        status = "4" if draft == "true" else "2"

        date_time = formatDateTime (request.POST.get ("datetime"))
        life_time = request.POST.get ("lifetime")

        #life_time = date_time
        if (life_time is None or life_time.strip() == ""):
            life_time = date_time
        else:
            life_time = formatDateTime (life_time)

        title     = request.POST.get ("title")
        category  = request.POST.get ("category")
        sub_category = request.POST.get ("sub_category")

        if request.POST.get ("title"):
            Response ({"error": "Param 'title' is required"})

        if request.POST.get ("category"):
            Response ({"error": "Param 'category' is required"})

        if request.POST.get ("desc") is None:
            desc = ""
        else:
            desc = request.POST.get ("desc")

        if request.POST.get ("price") is None:
            price = "0"
        else:
            price = request.POST.get ("price")

        if request.POST.get ("address") is None:
            address = ""
        else:
            address = request.POST.get ("address")

        phone     = request.user.userprofile.phone

        if (sub_category is None):
            sub_category = "0"

        if request.POST.get ("lat") is None:
            lat = 0.0
        else:
            lat = float (request.POST.get ("lat"))

        if request.POST.get ("lng") is None:
            lng = 0.0
        else:
            lng = float (request.POST.get ("lng"))

        date_time = date_time
        life_time = life_time

        if (draft != "true" and not request.user.userprofile.active):
            return Response ({"error": "U12"})

        Task.objects.filter (pk = tid).update (title = title, category = category, desc = desc, price = price,
                                    address = address, status = status, phone = phone, date_time = date_time,
                                    life_time = life_time, location = Point (lng, lat))

        Photo.objects.filter (task = task).delete()

        for i in range(4):
            photo = request.POST.get ("photo["+str(i)+"]")
            #thumb = request.POST.get ("thumb["+str(i)+"]")
            photo_url = request.POST.get ("photo_url["+str(i)+"]")

            if (photo):
                src   = upload_photo (photo)
                #thumb = upload_photo (thumb)

                Photo.objects.create (task = task, src = src)

            if (photo_url):
                src   = upload_photo_url (photo_url)
                #thumb = upload_photo     (thumb)
                Photo.objects.create (task = task, src = src)

        return Response ({"ok": True})
