"""

@api {post} /task/refuse/:id/ Отказаться от задачи
@apiName RefuseTask
@apiGroup Task
@apiDescription

Исполнитель отказывается от задания с `id` = `:id`. При этом задание
попадает в `открытые`, если время жизни еще не истекло.

@apiParam {Number} :id Уникальный идентификатор задачи

@apiSuccess {String} ok    True, если ошибок нет
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.utils                   import formats
from task.models                    import Task
from notification.models            import Notification

from datetime                       import timedelta, datetime
from django.utils                   import timezone

def check_lifetime():

    today       = timezone.now()
    tasks_query = Task.objects.filter (life_time__lt = today, status = '2')

    # кидаем уведомления хозяевам задачи

    for task in tasks_query.all():
        Notification.generate_expired (task)

    # Переносим просроченные задачи в черновик
    tasks_query.update(status = '4', last_status = '4')

class TaskRefuseView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, tid, format=None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user = request.user

        try:
            task = Task.objects.get (pk = tid)

        except Task.DoesNotExist:
            return Response ({"error" : "T0"})

        if (task.perfomer != user):
            return Response ({"error" : "T4"})

        if (task.status != "5"):
            return Response ({"error" : "T2"})


        task.perfomer = None
        task.completed = False
        task.status = "2"
        task.save()

        check_lifetime()
        Notification.generate_refuse(task)

        return Response ({"ok" : True})
