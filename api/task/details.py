"""
@api {get} /task/:id,:params/ Информация о задаче
@apiName GetTask
@apiGroup Task

@apiDescription
    Получение информации о задаче по уникальному идентификатору :id
    с параметрами :params.

@apiParam {Number} id         Уникальный идентификатор
@apiParam {String} params     Параметры отображения, через запятую:\
                                &nbsp;&nbsp;&nbsp;<b>user_detail</b> - отображение подробной информации об авторе задачи и исполнителе

@apiSuccess {Number}     id          Уникальный идентификатор задачи
@apiSuccess {User}       user        Уникальный идентификатор автора задачи или объект <a href = "#api-User-GetUser">User</a>, если включен параметр user_detail
@apiSuccess {User}       perfomer    Уникальный идентификатор исполнителя задачи или объект <a href = "#api-User-GetUser">User</a>, если включен параметр user_detail
@apiSuccess {[Object]}   photos      Список фотографий, `thumb` - миниатюра фотографии, `src` - полный размер фотографии
@apiSuccess {String}     title       Заголовок
@apiSuccess {String}     desc        Описание задачи
@apiSuccess {Number}     price       Цена
@apiSuccess {Number}     lat         Широта, где распологается маркер
@apiSuccess {Number}     lng         Долгота, где распологается маркер
@apiSuccess {Number}     category    Номер категории:\
    &nbsp;&nbsp;&nbsp;<b>0</b> - Бытовой ремонт\
    &nbsp;&nbsp;&nbsp;<b>1</b> - Курьерские услуги\
    &nbsp;&nbsp;&nbsp;<b>2</b> - Уборка и помощь по хозяйству\
    &nbsp;&nbsp;&nbsp;<b>3</b> - Грузоперевозки\
    &nbsp;&nbsp;&nbsp;<b>4</b> - Компьютерная помощь\
    &nbsp;&nbsp;&nbsp;<b>5</b> - Мероприятия и промо-акции\
    &nbsp;&nbsp;&nbsp;<b>6</b> - Удаленная работа\
    &nbsp;&nbsp;&nbsp;<b>7</b> - Дизайн\
    &nbsp;&nbsp;&nbsp;<b>8</b> - Web-разработка\
    &nbsp;&nbsp;&nbsp;<b>9</b> - Фото- и видео-услуги\
    &nbsp;&nbsp;&nbsp;<b>10</b> - Ремонт цифровой техники\
    &nbsp;&nbsp;&nbsp;<b>11</b> - Установка и ремонт техники\
    &nbsp;&nbsp;&nbsp;<b>12</b> - Красота и здоровье\
    &nbsp;&nbsp;&nbsp;<b>13</b> - Юридическая помощь\
    &nbsp;&nbsp;&nbsp;<b>14</b> - Пошив и ремонт одежды и аксессуаров
@apiSuccess {Number}     sub_category    Номер подкатегории:\
    &nbsp;&nbsp;&nbsp;<b>0</b> - Другое\
    &nbsp;&nbsp;&nbsp;<b>Бытовой ремонт</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Мелкий бытовой ремонт\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Услуги электрика\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Услуги сантехника\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Отделочные работы\
    &nbsp;&nbsp;&nbsp;<b>Курьерские услуги</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Пеший курьер\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Курьер с личным автомобилем\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Покупка и доставка\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Доставка еды из ресторанов и магазинов\
    &nbsp;&nbsp;&nbsp;<b>Уборка и помощь по хозяйству</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Лёгкая уборка\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Генеральная уборка\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Стирка\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Уборка мусора\
    &nbsp;&nbsp;&nbsp;<b>Грузоперевозки</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Переезды\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Пассажирские перевозки\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Междугородные перевозки\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Услуги грузчиков\
    &nbsp;&nbsp;&nbsp;<b>Компьютерная помощь</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Ремонт комьютеров и ноутбуков\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Установка и настройка операционных систем, программ\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Удаление вирусов, шифровщиков\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Настройка Интернета, wifi роутеров\
    &nbsp;&nbsp;&nbsp;<b>Мероприятия и промо-акции</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Раздача промо материалов\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Промоутер\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Ведущий, аниматор\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Расклейщик объявлений\
    &nbsp;&nbsp;&nbsp;<b>Удаленная работа</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Работа с текстом, копирайтинг, переводы\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Поиск и обработка информации\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Работа в Office программах\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Реклама и продвижение в Интернете\
    &nbsp;&nbsp;&nbsp;<b>Дизайн</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Логотипы, фирменный стиль\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Листовки, буклеты, визитки\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Дизайн сайтов\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - 3d графика, анимация\
    &nbsp;&nbsp;&nbsp;<b>Web-разработка</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Создание сайта\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Программирование\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Вёрстка\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Разработка программ и приложений\
    &nbsp;&nbsp;&nbsp;<b>Фото- и видео-услуги</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Фотосъёмка\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Видеосъёмка\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Обработка изображений и видео\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Видеопрезентации, заставки, слайдшоу\
    &nbsp;&nbsp;&nbsp;<b>Ремонт цифровой техники</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Телефоны и планшеты\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Аудиотехника\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Телевизоры и мониторы\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Автомобильная электроника\
    &nbsp;&nbsp;&nbsp;<b>Установка и ремонт техники</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Стиральные машины\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Холодильники и морозильные камеры\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Электрические плиты и панели\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Мелкая бытовая техника\
    &nbsp;&nbsp;&nbsp;<b>Красота и здоровье</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Парикмахерские услуги\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Маникюр и педикюр\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Косметология и макияж\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Персональный тренер\
    &nbsp;&nbsp;&nbsp;<b>Юридическая помощ</b> \
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Юридическая консультация\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Составление и проверка договоров\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Оформление документов\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Частный детектив\
    &nbsp;&nbsp;&nbsp;<b>Пошив и ремонт одежды и аксессуаров</b>\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1</b> - Ремонт обуви\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2</b> - Ремонт часов\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3</b> - Ремонт одежды\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>4</b> - Пошив одежды
@apiSuccess {String}     date_time   Когда необходимо выполнить
@apiSuccess {String}     life_time   Время жизни задачи
@apiSuccess {String}     address     Адрес
@apiSuccess {String}     phone       Контактный телефон, если пользователь уже интерисовался данным заданием, то отображается телефон, иначе поле будет пустым.
@apiSuccess {String}     status      Статус задачи:\
                                        &nbsp;&nbsp;&nbsp;<b>1</b> - Модерация\
                                        &nbsp;&nbsp;&nbsp;<b>2</b> - Открыто\
                                        &nbsp;&nbsp;&nbsp;<b>3</b> - Закрыто\
                                        &nbsp;&nbsp;&nbsp;<b>4</b> - Черновик\
                                        &nbsp;&nbsp;&nbsp;<b>5</b> - В работе\
                                        &nbsp;&nbsp;&nbsp;<b>6</b> - Выполнено
@apiSuccess {Boolean}    contract_price Цена договорная, если `true`
@apiSuccess {Boolean}    no_fact_addr Не требует фактического пребывание, если `true`
@apiSuccess {Boolean}    completed `true` если исполнитель отметил задание как выполненное

@apiError   {String}     error Код ошибки

@apiSuccessExample Пример

{
    "perfomer": 16,
    "lng": 82.944814,
    "lat": 55.024695,
    "category": 9,
    "photos": [
        {
            "thumb": "http://bizzi.pro/media/uploaded_image1451559422_112213_xRNGZ3Q.jpg",
            "src": "http://bizzi.pro/media/uploaded_image1451559422_016144.jpg"
        }
    ],
    "my": false,
    "phone": "",
    "id": 5,
    "user": 3,
    "status": 6,
    "title": "сфотать свадьбу",
    "price": 0,
    "contract_price": true,
    "desc": "у меня свадьба. нужен оператор",
    "address": "Новосибирск, ул. Лескова, 15",
    "no_fact_addr": false,
    "life_time": "30.12.2015 (10:01)",
    "date_time": "30.12.2016 (10:01)"
}

@apiSuccessExample Пример c параметром user_detail
{
    "perfomer": {
        "my_reviews": 1,
        "rank": 5,
        "birthdate": "",
        "phone": "79139349968",
        "gender": "m",
        "active": true,
        "tasks_count": 1,
        "type": 2,
        "id": 16,
        "about_me_reviews": 1,
        "lastname": "Белов",
        "email": "cultire@gmail.com",
        "city": "Новосибирск",
        "firstname": "Александр",
        "skills": "",
        "start": 0,
        "avatar": "http://bizzi.pro/media/uploaded_image1451978125_4634757_1VmcaYO.jpg",
        "subscription": [],
        "date_joined": "января 2016"
    },
    "lng": 82.944814,
    "lat": 55.024695,
    "category": 9,
    "photos": [
        {
            "thumb": "http://bizzi.pro/media/uploaded_image1451559422_112213_xRNGZ3Q.jpg",
            "src": "http://bizzi.pro/media/uploaded_image1451559422_016144.jpg"
        }
    ],
    "my": false,
    "phone": "",
    "id": 5,
    "user": {
        "my_reviews": 1,
        "rank": 4,
        "birthdate": "",
        "phone": "79529303040",
        "gender": "m",
        "active": true,
        "tasks_count": 1,
        "type": 1,
        "id": 3,
        "about_me_reviews": 2,
        "lastname": "Зуев",
        "email": "",
        "city": "Новосибирск",
        "firstname": "Алексей",
        "skills": "",
        "start": 0,
        "avatar": "http://cs320527.vk.me/v320527547/69ad/XTuwT9-2hCQ.jpg",
        "subscription": [],
        "date_joined": "декабря 2015"
    },
    "status": 6,
    "title": "сфотать свадьбу",
    "price": 0,
    "contract_price": true,
    "desc": "у меня свадьба. нужен оператор",
    "address": "Новосибирск, ул. Лескова, 15",
    "no_fact_addr": false,
    "life_time": "30.12.2015 (10:01)",
    "date_time": "30.12.2016 (10:01)"
}

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.utils                   import formats
from task                           import models
from notification.models            import Notification

from api.user.details               import get_profile
from datetime                       import timedelta, datetime
from django.utils                   import timezone
from django.db.models               import Q

from sorl.thumbnail import get_thumbnail

def get_categoty (category):
    if (category):
        if (category.icon): icon_url = "{0}{1}".format("http://bizzi.pro", category.icon)
        else:               icon_url = ""

        return {
            'id'     : category.id,
            'title'  : category.title,
            'icon'   : icon_url,
            'parent' : get_categoty (category.parent)
        }

    return None

def get_categories (categories):
    cats = []

    for category in categories.all():
        cats.append (get_categoty(category))

    return cats

def get_photos (task):
    #print (task.photo_set.all)
    photos = []

    for photo in task.photo_set.all():
        src_im = get_thumbnail(photo.src, '1280x1024', crop='center', quality=99)
        thumb_im = get_thumbnail(photo.src, '200x200', crop='center', quality=70)

        url   = "{0}{1}".format("http://bizzi.pro", src_im  .url) if photo.src else ""
        thumb = "{0}{1}".format("http://bizzi.pro", thumb_im.url) if photo.src else ""

        photos.append ({"src":url, "thumb":thumb})

    return photos

def get_task (task, user_details, myuser = None):

    if (myuser and myuser.is_authenticated()):
        myuser.userprofile.old_tasks.add (task)

    user     = task.user
    perfomer = task.perfomer
    #photo  = "{0}{1}".format ("http://bizzi.pro", task.photo.url) if task.photo else ""
    user_content = get_profile (user) if user_details else user.id

    if (perfomer):
        prefomer_content = get_profile (perfomer) if user_details else perfomer.id
    else:
        prefomer_content = None

    phone = ""

    if (myuser.id and task in myuser.userprofile.showed_tasks.all()):
        phone = task.phone

    life_time = formats.date_format (task.life_time, "DATETIME_FORMAT")
    date_time = formats.date_format (task.date_time, "DATETIME_FORMAT")

    if (date_time == "01.01.1800 (00:00)"):
        date_time = ""

    if (life_time == "01.01.1800 (00:00)"):
        life_time = ""

    return {
        'id'             : task.id,
        'user'           : user_content,
        'perfomer'       : prefomer_content,
        'photos'         : get_photos (task),
        'title'          : task.title,
        'category'       : int(task.category),
        'sub_category'   : int(task.sub_category),
        'desc'           : task.desc,
        'price'          : task.price,
        'date_time'      : date_time,
        'life_time'      : life_time,
        'address'        : task.address,
        'phone'          : phone,
        'status'         : int(task.status),
        'lat'            : task.location[1],
        'lng'            : task.location[0],
        'my'             : myuser == task.user,
        'no_fact_addr'   : task.address == "",
        'contract_price' : task.price == 0,
        'completed'      : task.completed,
    }

def get_task_short (task, myuser = None):

    return {
        'id'             : task.id,
        'title'          : task.title,
        'price'          : task.price,
        'status'         : int(task.status),
        'category'       : int(task.category),
        'lat'            : task.location[1],
        'lng'            : task.location[0],
        'my'             : myuser == task.user,
        'no_fact_addr'   : task.address == "",
    }

def check_lifetime():

    today       = timezone.now()
    tasks_query = models.Task.objects.filter (life_time__lt = today, status = '2')

    # кидаем уведомления хозяевам задачи

    for task in tasks_query.all():
        Notification.generate_expired (task)

    # Переносим просроченные задачи в черновик
    tasks_query.update(status = '4', last_status = '4')

class TaskView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, tid, format=None):

        content = {}

        try:
            task    = models.Task.objects.get (pk = tid)
            user    = task.user
            content = get_task (task, False, request.user)

        except models.Task.DoesNotExist:
            content = {"error": "T0"}

        return Response (content)

class TaskUserView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, tid, format=None):

        content = {}

        try:
            task    = models.Task.objects.get (pk = tid)
            user    = task.user
            content = get_task (task, True, request.user)

        except models.Task.DoesNotExist:
            content = {"error": "T0"}

        return Response (content)

"""
@api {get} /tasks[:offset,:limit]/:params/ Список задач
@apiName GetTasks
@apiGroup Task

@apiDescription
    Получения списка задач. Необязательные параметры :offset, :limit и :params\
    \
    Пример запроса: `/tasks/`\
    Вывести все задачи.\
    \
    Пример запроса: `/tasks[0,5]/`\
    Вывести задачи, максимальное количество задач - 10;\
    \
    Пример запроса: `/tasks[5,10]/my/`\
    Вывести задачи авторизованного пользователя, пропустив первые 5 задач,\
    максимальное количество задач - 10;\
    \
    Пример запроса: `/tasks,short/`\
    Вывести все задачи, с неполными параметрами.\
    С флагом `short` выводятся только поля: `id`; `status`; `title`; `price`; `my`; `lat`; `lng`; `no_fact_addr`; `category`.\
    \
    Пример запроса: `/tasks,short,zip/`\
    Вывести все задачи в сжатом виде, сжатие через zip, с неполными параметрами.

@apiParam {Number} offset     Количество пропущенных задач
@apiParam {Number} limit      Максимальное количество задач
@apiParam {String} params     Параметры отображения, через запятую:\
                                &nbsp;&nbsp;&nbsp;<b>my</b> - отображать только задачи авторизованного пользователя

@apiSuccess {[Task]} tasks Список <a href = "#api-Task-GetTask">задач</a>
@apiSuccess {String} zip Сжатый JSON, список всех задач
@apiError   {String} error Код ошибки

@apiSuccessExample Пример с флагом "short"
{
    "tasks": [
        {
            "status": 2,
            "title": "создать дизайн для кластера",
            "price": 25000,
            "my": false,
            "lat": 0.0,
            "no_fact_addr": true,
            "lng": 0.0,
            "id": 20,
            "category": 7
        }
    ]
}

@apiSuccessExample Пример с флагами "short,zip"
{
    "zip": "b'eNo9jD0KwkAQha+y3TYiS0AED+AJ7ETCsIkhGBPZnRQSAia14FVSKP4EPcPsjZyxsPu+ee9NoxH8zuuFWjfaI2AtHE2UxhyLlFmHjj50pysNoQ9nxfBgHehJb5ExXBS9aOS4Cz3dwokGzfuDy63so5kxhn1/ZFlC4VOWApDNTCUoq3gLFmNIEsfHlat/jTL7N/JE/ghZwDSrnLyat5v2C0AlS1k='"
}

"""

class TasksMyAllView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format=None):

        if (request.user.id):
            user          = request.user
            tasks         = models.Task.objects.filter(Q(user = user) | Q(perfomer = user)).order_by("-date_time")
            tasks_content = []

            for task in tasks:
                tasks_content.append (get_task(task, False, request.user))

            content = {"tasks": tasks_content}

        else:
            content = {"error": "U0"}

        return Response (content)


class TasksMyView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):

        if (request.user.id):
            user          = request.user
            #tasks         = models.Task.objects.filter(user = user).order_by("-date_time")[int(offset):int(offset)+int(limit)]

            if (user.userprofile.utype == "1"):
                tasks = models.Task.objects.filter (user = user).order_by("-date_time")[int(offset):int(offset)+int(limit)]
            else:
                tasks = models.Task.objects.filter (perfomer = user).order_by("-date_time")[int(offset):int(offset)+int(limit)]

            tasks_content = []

            for task in tasks:
                tasks_content.append (get_task(task, False, request.user))

            content = {"tasks": tasks_content}

        else:
            content = {"error": "U0"}

        return Response (content)

class TasksView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):

        tasks         = models.Task.objects.filter(status = "2").order_by("-date_time")[int(offset):int(offset)+int(limit)]
        tasks_content = []
        check_lifetime()
        user_details = request.GET.get("user_details")
        user_details_b = False

        if (user_details != None):
            user_details_b = user_details == "true" or user_details == "1"

        for task in tasks:
            tasks_content.append (get_task(task, user_details_b, request.user))

        content = {"tasks": tasks_content}
        return Response (content)

class TasksAllView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format=None):

        tasks         = models.Task.objects.filter(status = "2").order_by("-date_time")
        tasks_content = []
        check_lifetime()
        user_details = request.GET.get("user_details")
        user_details_b = False

        if (user_details != None):
            user_details_b = user_details == "true" or user_details == "1"

        for task in tasks:
            tasks_content.append (get_task(task, user_details_b, request.user))

        content = {"tasks": tasks_content}
        return Response (content)

class TasksShortView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format=None):

        tasks         = models.Task.objects.filter(status = "2").order_by("-date_time")
        tasks_content = []
        check_lifetime()

        for task in tasks:
            tasks_content.append (get_task_short(task, request.user))

        content = {"tasks": tasks_content}
        return Response (content)

import zlib
import base64

class TaskShortZipView (APIView):

    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format=None):
        tasks         = models.Task.objects.filter(status = "2").order_by("-date_time")
        tasks_content = []
        check_lifetime()

        for task in tasks:
            tasks_content.append (get_task_short(task, request.user))

        content    = str({"tasks": tasks_content})
        compressed = zlib.compress(str.encode(content), 9)

        return Response ({"zip" : str(base64.b64encode(compressed))})
