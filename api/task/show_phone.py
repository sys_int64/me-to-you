"""

@api {get} /task/show-phone/:id/ Запросить телефон для задания
@apiName ShowPhoneTask
@apiGroup Task
@apiDescription

Если у пользователя не исчерпался лимит на отображение задач, то для задачи с уникальным идентификтором `:id` метод вернет номер телефона заказчика
и уведомит его о том, что пользователь заинтерисовался заданием.

@apiParam {Number} :id Уникальный идетификатор задачи

@apiSuccess {String} phone Номер телефона автора задания
@apiError   {String} error Код ошибки

@apiSuccessExample Пример
{
    "phone": "+79137698347"
}

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from task.models                    import Task
from django.contrib.auth.models     import User
from notification.models            import Notification
import datetime

class ShowTaskPhoneView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, tid, format=None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        try:
            task = Task.objects.get (pk = tid)

        except Task.DoesNotExist:
            return Response ({"error" : "T0"})

        user = request.user

        #if (user.userprofile.tasks_shows >= 3): TODO: Uncoment some day
        #    return Response ({"error" : "U13"})

        user.userprofile.tasks_shows += 1
        user.userprofile.showed_tasks.add (task)
        user.userprofile.save()

        # Notification.generate_give(task)
        Notification.generate_interested(user, task)
        return Response ({"phone" : task.phone})
