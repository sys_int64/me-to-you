"""

@api {post} /task/close/:id/ Отметить как выполненное
@apiName CloseTask
@apiGroup Task
@apiDescription

Отметить задания с `id` = `:id` как выполненное.

@apiParam {Number} :id Уникальный идентификатор задачи

@apiSuccess {String} ok    True, если ошибок нет
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.utils                   import formats
from task                           import models
from notification.models            import Notification

class TaskCloseView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, tid, format=None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        user = request.user

        try:
            task = models.Task.objects.get (pk = tid)

        except models.Task.DoesNotExist:
            return Response ({"error" : "T0"})

        if (task.user != user):
            return Response ({"error" : "T1"})

        if (task.status != "5"):
            return Response ({"error" : "T2"})

        task.status      = "6"
        task.last_status = "6"
        task.save()

        Notification.generate_estimate (task)
        return Response ({"ok": True})
