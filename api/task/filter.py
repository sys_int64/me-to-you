"""

@api {get} /tasks/filter/[:offset,:limit]/:params/ Фильтр задач
@apiName FilterTask
@apiGroup Task
@apiDescription

`:offset`; `:limit`; `:params` являются не обязательными полями.\
Фильтрует задачи. Если фильтровать только мои задачи, то в cats вписываются значения `status`, по которым фильтровать <a href = "#api-Task-GetTask">задачи</a>.
Иначе фитрация по категориям.\
Примеры запросов:\
&nbsp;&nbsp;&nbsp;`/tasks/filter/?cats=1,2,3&city=Новосибирск` - Отфильтровать записи, и отобразить все отфильтрованные записи.\
&nbsp;&nbsp;&nbsp;`/tasks/filter[0:10]/?cats=1,2,3&city=Новосибирск` - Отфильтровать записи, и отобразить с 0 по 10 записи.\
&nbsp;&nbsp;&nbsp;`/tasks/filter,short/?cats=1,2,3&city=Новосибирск` - Отфильтровать записи, и отобразить все отфильтрованные записи в кратком содержании.\
&nbsp;&nbsp;&nbsp;`/tasks/filter,short/my/?cats=1,2,3&city=Новосибирск` - Отфильтровать мои записи, и отобразить все отфильтрованные записи в кратком содержании.

@apiParam {Number} offset     Количество пропущенных задач
@apiParam {Number} limit      Максимальное количество задач
@apiParam {String} params     Параметры отображения, через запятую:\
                                &nbsp;&nbsp;&nbsp;<b>my</b> - фильтровать только задачи авторизованного пользователя
@apiParam {String} cats По каким категориям показывать задачи, строка, через запятую, пример: `1,3,6`
@apiParam {String} city Город, в котором находятся маркеры

@apiSuccess {[Task]} tasks Список найденых <a href = "#api-Task-GetTask">задач</a>
@apiError   {String} error Код ошибки

@apiSuccessExample Пример
{
    "tasks": [
        {
            "perfomer": null,
            "lng": 0.0,
            "lat": 0.0,
            "category": 7,
            "photos": [
                {
                    "thumb": "http://me2you.pro/media/uploaded_image1455351818_1100883_kdFsne8.jpg",
                    "src": "http://me2you.pro/media/uploaded_image1455351817_9023857.jpg"
                }
            ],
            "my": false,
            "phone": "",
            "id": 20,
            "user": 25,
            "status": 2,
            "title": "создать дизайн для сайта",
            "price": 25000,
            "contract_price": false,
            "desc": "срочно",
            "address": "",
            "no_fact_addr": true,
            "life_time": "22.02.2016 (14:25)",
            "date_time": "22.02.2016 (14:23)"
        }
    ]
}

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.utils                   import formats
from task                           import models
from api.task.details               import get_task

from django.db.models import Q
import string

class FilterTaskView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):

        city = request.GET.get ("city");
        cats = request.GET.get ("cats");
        sub_cats = request.GET.get ("sub_cats");

        query = Q();

        if (city is not None):
            query = Q(address__icontains = city)

        if (cats is not None):
            cats = cats.split (",")

            for cat in cats:
                query = query | Q(category = cat)

        if (sub_cats is not None):
            sub_cats = sub_cats.split (",")

            for cat in sub_cats:
                cat = cat.split ("_")
                query = query | (Q(category = cat[0]) & Q(sub_category = cat[1]))

        tasks = models.Task.objects.filter (Q(status = "2") & query).order_by("-date_time")[int(offset):int(offset)+int(limit)]
        tasks_content = []

        for task in tasks:
            tasks_content.append (get_task(task, False, request.user))

        content = {"tasks": tasks_content}
        return Response (content)

class FilterMyTaskView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):

        if (not request.user.id):
            return Response ({"error": "U0"})

        user = request.user
        cats = request.GET.get ("cats");

        query = Q();

        if (cats):
            cats = cats.split (",")

            for cat in cats:
                query = query | Q(status = cat)

        if (user.userprofile.utype == "1"):
            myquery = Q(user = user)
        else:
            myquery = Q(perfomer = user)

        tasks = models.Task.objects.filter (myquery & query).order_by("-date_time")[int(offset):int(offset)+int(limit)]
        tasks_content = []

        for task in tasks:
            tasks_content.append (get_task(task, False, request.user))

        content = {"tasks": tasks_content}
        return Response (content)

class FilterTaskAllView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, format=None):

        city = request.GET.get ("city");
        cats = request.GET.get ("cats");

        query = Q();

        if (city):
            query = Q(address__icontains = city)

        if (cats):
            cats = cats.split (",")

            for cat in cats:
                query = query | Q(category = cat)

        tasks = models.Task.objects.filter (Q(status = "2") & query).order_by("-date_time")

        tasks_content = []

        for task in tasks:
            tasks_content.append (get_task(task, False, request.user))

        content = {"tasks": tasks_content}
        return Response (content)
