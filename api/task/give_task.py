"""

@api {post} /task/give-task/:id/ Отдать задачу на исполнение
@apiName GiveTask
@apiGroup Task
@apiDescription

Отдать задачу с уникальным идентификтором `:id` на исполнение пользователю с `id` = `perfomer_id`.

@apiParam {Number} :id Уникальный идетификатор задачи
@apiParam {Number} perfomer_id Уникальный идетификатор исполнителя

@apiSuccess {Boolean} ok `true` если все хорошо
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.utils                   import formats
from task                           import models
from notification.models            import Notification

class GiveTaskView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, tid, format=None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        # Get Task

        try:
            task = models.Task.objects.get (pk = tid)

        except models.Task.DoesNotExist:
            return Response ({"error": "T0"})

        # Get Perfomer

        perfomer_id = int(request.POST.get ("perfomer_id"))

        try:
            perfomer = User.objects.get (pk = perfomer_id)

        except User.DoesNotExist:
            return Response ({"error": "U0"})

        # Set Pefomer to Task

        user = request.user

        if (user != task.user):
            return Response ({"error": "T?"}) # TODO: Set Error Number

        # Notification.generate_give_task(task, user)
        task.perfomer = perfomer
        task.status = "5"
        task.save()

        Notification.generate_give (task)
        return Response ({"ok": True})
