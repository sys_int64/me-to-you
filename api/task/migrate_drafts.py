from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from task.models import Task, Photo
from django.contrib.gis.geos import Point
from time import time
from django.core.files.base import ContentFile
import os
from base64 import b64decode
from django.conf import settings

def formatDate (date):
    pass

def formatDateTime (datetime):
    if (datetime.strip() == ""):
        return "1800-01-01 00:00:00"
    else:
        parts = datetime.split (" ")
        date  = parts[0].split (".")
        time  = parts[1].split (":")

        return date[2]+"-"+date[1]+"-"+date[0]+" "+time[0]+":"+time[1]+":00"

def uploadPhoto (base64_string, task):

    decoded_image = b64decode (base64_string)
    filename = "uploaded_image%s.jpg" % str(time()).replace('.','_')

    fh = open (os.path.join (settings.MEDIA_ROOT, filename), "wb")
    fh.write  (decoded_image)
    fh.close()

    #photo = Photo.objects.create (task = task, src = ContentFile (decoded_image, filename))
    return ContentFile (decoded_image, filename)

class MigrateDraftsView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def post (self, request, format=None):

        if (not request.user.id):
            return Response ({"error" : "U0"})

        count  = int (request.POST.get ("items_count"))
        status = "4"

        for i in range(count):
            istr      = str(i)

            title     = request.POST.get ("title_"   + istr)
            category  = request.POST.get ("cat_"     + istr)
            desc      = request.POST.get ("desc_"    + istr)
            price     = request.POST.get ("price_"   + istr)
            address   = request.POST.get ("address_" + istr)
            phone     = request.user.userprofile.phone

            lat       = float (request.POST.get ("lat_" + istr))
            lng       = float (request.POST.get ("lng_" + istr))

            date_time = formatDateTime (request.POST.get ("datetime_" + istr))
            life_time = formatDateTime (request.POST.get ("lifetime_" + istr))

            task = Task.objects.create (user = request.user, title = title, category = category, desc = desc, price = price,
                                        address = address, status = status, phone = phone, date_time = date_time,
                                        life_time = life_time, location = Point (lng, lat))

            for j in range(4):
                photo = request.POST.get ("photo_"+istr+"_"+str(j))
                thumb = request.POST.get ("thumb_"+istr+"_"+str(j))
                #print (photo)

                if (photo and thumb):
                    thumb_c = uploadPhoto (thumb, task)
                    photo_c = uploadPhoto (photo, task)

                    photo = Photo.objects.create (task = task, src = photo_c, thumb = thumb_c)

        return Response ({"ok": True})
