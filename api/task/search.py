"""

@api {get} /tasks/search/[:offset,:limit]/:params/ Поиск задач
@apiName SearchTask
@apiGroup Task
@apiDescription

Ищет задачи с ключем `key` и со статусом `открыто`.

@apiParam {Number} offset     Количество пропущенных задач
@apiParam {Number} limit      Максимальное количество задач
@apiParam {String} params     Параметры отображения, через запятую:\
                                &nbsp;&nbsp;&nbsp;<b>my</b> - отображать только задачи авторизованного пользователя
@apiParam {String} key Ключевые слова для поиска

@apiSuccess {[Task]} tasks Список найденых <a href = "#api-Task-GetTask">задач</a>
@apiError   {String} error Код ошибки

@apiSuccessExample Пример
{
    "tasks": [
        {
            "perfomer": null,
            "lng": 0.0,
            "lat": 0.0,
            "category": 7,
            "photos": [
                {
                    "thumb": "http://me2you.pro/media/uploaded_image1455351818_1100883_kdFsne8.jpg",
                    "src": "http://me2you.pro/media/uploaded_image1455351817_9023857.jpg"
                }
            ],
            "my": false,
            "phone": "",
            "id": 20,
            "user": 25,
            "status": 2,
            "title": "создать дизайн для сайта",
            "price": 25000,
            "contract_price": false,
            "desc": "срочно",
            "address": "",
            "no_fact_addr": true,
            "life_time": "22.02.2016 (14:25)",
            "date_time": "22.02.2016 (14:23)"
        }
    ]
}

"""

from rest_framework.permissions     import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response        import Response
from rest_framework.views           import APIView

from django.contrib.auth.models     import User
from django.utils                   import formats
from task                           import models
from api.task.details               import get_task
from django.db.models               import Q

class SearchTaskView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):

        key   = request.GET.get ("key");
        tasks = models.Task.objects.filter (Q(status = "2") &
            (Q(title__icontains = key) | Q(address__icontains = key) | Q(desc__icontains = key))
            ).order_by("-date_time")[int(offset):int(offset)+int(limit)]

        tasks_content = []

        for task in tasks:
            tasks_content.append (get_task(task, False, request.user))

        content = {"tasks": tasks_content}
        return Response (content)

class SearchMyTaskView (APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get (self, request, offset, limit, format=None):

        if (not request.user.id):
            return Response ({"error": "U0"})

        user = request.user

        if (user.userprofile.utype == "1"):
            myquery = Q(user = user)
        else:
            myquery = Q(perfomer = user)

        key   = request.GET.get ("key");
        tasks = models.Task.objects.filter (myquery &
            (Q(title__icontains = key) | Q(address__icontains = key) | Q(desc__icontains = key))
            ).order_by("-date_time")[int(offset):int(offset)+int(limit)]

        tasks_content = []

        for task in tasks:
            tasks_content.append (get_task(task, False, request.user))

        content = {"tasks": tasks_content}
        return Response (content)
