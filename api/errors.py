
"""
@api {errors} / Коды ошибок User
@apiName UserErrors
@apiGroup Errors

@apiDescription
    <style>pre.language-html[data-type="errors"]:before {background-color: #ed0039;}</style>

@apiError U0 Пользователь не найден
@apiError U1 Пользователь уже активирован
@apiError U2 Проверочный ключ не действителен, обновите ключ
@apiError U3 Ключ введен не верно
@apiError U4 Неверно введен старый пароль
@apiError U5 Пользователь с таким телефоном уже зарегистрирован в системе
@apiError U6 Неверный хост для Digits
@apiError U7 Ошибка при подтверждении секретного кода от Digits, возвращает так же `twitter_code` вместе с ошибкой
@apiError U8 Не удалось обновить/подтвердить телефон
@apiError U9 У пользователя уже установлена роль
@apiError U10 Неизвестаная роль для пользователя
@apiError U11 Оперция не позволеня для пользователя зарегистрированного через социальную сеть
@apiError U12 Пользователь не может размещать задание с не подтвержденным номером телефона
@apiError U13 Исчерпан лимит на показ телефонов для задания
@apiError U14 Пользователь с таким e-mail уже зарегистрирован в системе
@apiError U15 Значение имеет неверный формат даты. Оно должно быть в формате DD.MM.YYYY"

"""

"""
@api {errors} / Коды ошибок Task
@apiName TaskErrors
@apiGroup Errors

@apiDescription
    <style>pre.language-html[data-type="errors"]:before {background-color: #ed0039;}</style>

@apiError T0 Задача не найдена
@apiError T1 Пользователь не является собственником задачи
@apiError T2 Статус задачи должен быть `В работе`
@apiError T3 Задание уже выполнено
@apiError T4 Пользователь не является исполнителем для данной задачи

"""
