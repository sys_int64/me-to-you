from django.contrib import admin
from .models import Notification

class NotificationAdmin (admin.ModelAdmin):
    raw_id_fields = ('user', 'task',)
    #exclude = ('user', 'task', 'status', 'system', 'cat', 'date_time', 'sended', 'readed')
    filter_horizontal = ('to',)

admin.site.register (Notification, NotificationAdmin)
