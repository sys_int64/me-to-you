from django.db import models

from django.contrib.auth.models import User
import datetime
from task.models import Task

NOTIFY_STATUS_CHOICES = (
    ('0', 'Не задан'   ), ('1', 'Одобрен'), ('2', 'Отклонен'),
    ('3', 'Взять заказ'), ('4', 'Оценить'), ('5', 'Новое задание'),
)

ROLE_STATUS_CHOICES = (
    ('0', 'Любая'), ('1', 'Заказчики'), ('2', 'Исполнители'),
)

class Notification (models.Model):

    class Meta:
        verbose_name        = 'Уведомление'
        verbose_name_plural = 'Уведомления'

    to_users  = models.BooleanField    ("Получатели", default = False)
    to        = models.ManyToManyField (User, related_name = "to"     , verbose_name = "Получатели"  , default = None, blank = True)
    exclude   = models.ManyToManyField (User, related_name = "exclude", verbose_name = "Исключить"   , default = None, blank = True)
    user      = models.ForeignKey      (User, related_name = "user"   , verbose_name = "Пользователь", default = None, blank = True, null = True)
    task      = models.ForeignKey      (Task, related_name = "task"   , verbose_name = "Задача"      , default = None, blank = True, null = True)

    title     = models.CharField       ("Название"       , max_length = 200)
    body      = models.TextField       ("Сообщение"      , blank = True)
    date_time = models.DateTimeField   ("Время и дата"   , blank = True, default   = datetime.datetime.now)
    status    = models.CharField       ("Статус"         , max_length = 1, choices = NOTIFY_STATUS_CHOICES, default = '0')
    role      = models.CharField       ("Роль"           , max_length = 1, choices = ROLE_STATUS_CHOICES  , default = '0')
    system    = models.BooleanField    ("Системное"      , default = False)
    sended    = models.BooleanField    ("Было отправлено", default = False)
    readed    = models.BooleanField    ("Было прочитано" , default = False)

    @staticmethod
    def generate_interested (i_user, task):
        title = i_user.first_name+" "+i_user.last_name
        body  = "Заинтересовался заданием №"+str(task.id)+": \""+task.title+"\""

        notification = Notification.objects.create (to_users = True, task = task,
            user = i_user, title = title, body = body, status = 3, system = False)

        notification.to.add (task.user)
        #notification.to.add (i_user)

    @staticmethod
    def generate_give (task):
        title = "Задание отдано"
        body  = "Задание №"+str(task.id)+": \""+task.title+"\" отдано Вам на исполнение"

        notification = Notification.objects.create (to_users = True, task = task,
            user = task.user, title = title, body = body, status = 1, system = False)

        notification.to.add (task.perfomer)

    @staticmethod
    def generate_estimate (task):
        title = task.user.first_name+" "+task.user.last_name
        body  = "Оцените пользователя за выполнение задания №"+str(task.id)+": "+task.title

        notification = Notification.objects.create (to_users = True, task = task,
            user = task.user, title = title, body = body, status = 4, system = False)

        notification.to.add (task.perfomer)

    @staticmethod
    def generate_accept (task):
        title = "Задание одобрено"
        body  = "Задание №"+str(task.id)+": \""+task.title+"\" одобрено модератором"

        notification = Notification.objects.create (to_users = True, task = task,
            title = title, body = body, status = 1, system = True)

        notification.to.add (task.user)

    @staticmethod
    def generate_reject (task):
        title = "Задание отклонено"
        body  = "Задание №"+str(task.id)+": \""+task.title+"\" отклонено модератором и отправлено в черновик"

        notification = Notification.objects.create (to_users = True, task = task,
            title = title, body = body, status = 2, system = True)

        notification.to.add (task.user)

    @staticmethod
    def generate_new_task (task, to_user):
        title = "Новое задание"
        body  = "Задание №"+str(task.id)+": \""+task.title+"\""

        notification = Notification.objects.create (to_users = True, task = task,
            title = title, body = body, status = 5, system = True)

        notification.to.add (to_user)
        #pass

    @staticmethod
    def generate_expired (task):
        title = "Истекло время"
        body  = "Срок действия задания №"+str(task.id)+": \""+task.title+"\" истекло, оно отправлено в черновик"

        notification = Notification.objects.create (to_users = True, task = task,
            title = title, body = body, status = 2, system = True)

        notification.to.add (task.user)

    @staticmethod
    def generate_refuse (task):
        title = "Отказ"
        body  = "Исполнитель отказался от задания №"+str(task.id)+": \""+task.title+"\""

        notification = Notification.objects.create (to_users = True, task = task,
            title = title, body = body, status = 2, system = True)

        notification.to.add (task.user)

    @staticmethod
    def generate_complete (task):
        title = "Выполнено"
        body  = "Исполнитель выполнил задание №"+str(task.id)+": \""+task.title+"\""

        notification = Notification.objects.create (to_users = True, task = task,
            title = title, body = body, status = 1, system = True)

        notification.to.add (task.user)

    def __str__ (self):
        return self.title

from django.db.models.signals import post_save
import requests
import json
from datetime     import timedelta
from django.utils import timezone
from django.db.models.signals import m2m_changed, post_save

def send_notification (to, data):
    params = {
        "to": to,
        "data": data
    }

    head = {
        "Authorization" : "key=AIzaSyC7IfRcX07eRHIt91BieQ4GnLMg7G_oARo",
        "Content-Type"  : "application/json"
    }

    #print (params)
    r = requests.post ("https://android.googleapis.com/gcm/send", data = json.dumps (params), headers = head)

    head = {
        "Authorization" : "key=AIzaSyAufkN9PP37H78nRoy43Di0FzMHUFSAVTE",
        "Content-Type"  : "application/json"
    }

    #print (params)
    r = requests.post ("https://android.googleapis.com/gcm/send", data = json.dumps (params), headers = head)

def create_notifications (sender, action, using, **kwargs):
    notification = kwargs["instance"]

    if (not notification.to_users):
        pass
    else:
        if (action != "post_add"):
            return

    if (notification.sended):
        return

    to = "/topics/global"
    ignore = False;

    if (notification.role == "1"):
        to = "/topics/perfomers"
    elif (notification.role == "2"):
        to = "/topics/customers"

    items = notification.to.all()

    for user in items:
        ignore = True
        token = user.userprofile.gcm_token
        send_notification (token, {"message": notification.body, "title": notification.title})

    notification.sended = True
    notification.save()

    # Удаление из базы данных "мертвых" уведомлений
    Notification.objects.filter (readed = True).delete()
    Notification.objects.filter (date_time__lte = timezone.now()-timedelta (days = 30)).delete() # Старше 30 дней

    if (not ignore):
        send_notification (to, {"message": notification.body, "title": notification.title})

def post_save_task (sender, **kwargs):
    task = kwargs["instance"]

    if (task.last_status == task.status):
        return

    task.last_status = task.status
    task.save()

    if (task.status == "2"):
        Notification.generate_accept (task)

    if (task.status == "4"):
        Notification.generate_reject (task)

m2m_changed.connect (create_notifications, sender = Notification.to.through)
post_save.connect   (post_save_task, sender = Task)
