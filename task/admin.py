from django.contrib import admin
from .models import Task, Photo
from feincms.admin import tree_editor

class PhotoInline(admin.StackedInline):
    model   = Photo
    extra   = 1
    max_num = 4

"""
class TaskCategoryAdmin (tree_editor.TreeEditor):
    raw_id_fields     = ('parent',)
    list_display      = ('title',)
"""

from django.conf.urls import patterns, url, include
from django.contrib import messages
from django import http
from django.core.urlresolvers import reverse
from notification.models import Notification
import urllib.parse

def make_accepted (modeladmin, request, queryset):
    #if (queryset.status == '1'):
    messages.success (request, "Задачи одобрены")
    queryset.update (status = '2')

def make_draft (modeladmin, request, queryset):
    #if (queryset.status == '1'):
    messages.success (request, "Задачи отклонены")
    queryset.update (status = '4')

make_accepted.short_description = "Одобрить"
make_draft   .short_description = "Отколнить"

#def custom_redirect (url_name, **kwargs):
#    url = reverse(url_name)
#    params = urllib.parse.urlencode(kwargs)
#    return HttpResponseRedirect(url + "?%s" % params)

class TaskAdmin (admin.ModelAdmin):
    #filter_horizontal = ('categories',)
    list_display      = ('title', 'date_time', 'status', 'actions_button')
    raw_id_fields     = ('user',)
    list_filter       = ['date_time', 'status']
    search_fields     = ['title', 'desc']
    inlines           = [PhotoInline]
    actions           = [make_accepted, make_draft]

    def changelist_view(self, request, extra_context=None):
        self.request = request
        return super(TaskAdmin,self).changelist_view(request, extra_context=extra_context)

    def get_params (self, request):
        params = []
        request.GET._mutable = True

        for k,vals in request.GET.lists():
            params.append ((k,vals[0]))

        return urllib.parse.urlencode(params)

    def actions_button (self, obj):

        if (obj.status == "1"):
            return '<a href="accept/%s/?%s">Одобрить</a> | <a href="to_draft/%s/?%s">Отклонить</a>' % (obj.id, self.get_params(self.request), obj.id, self.get_params(self.request))

        if (obj.status != "3"):
            return '<a href = "close/%s/?%s">Закрыть</a>' % (obj.id, self.get_params(self.request))

        return ""

    actions_button.allow_tags = True
    actions_button.short_description = 'Действие'

    def get_urls (self):
        urls = super(TaskAdmin, self).get_urls()
        my_urls = patterns('',
            (r'^accept/(?P<id>\d)/$'  , self.accept),
            (r'^to_draft/(?P<id>\d)/$', self.to_draft),
            (r'^close/(?P<id>\d)/$'   , self.close),
        )

        return my_urls + urls

    def accept (self, request, id):
        #Task.objects.filter (id = id).update (status = "2")
        task = Task.objects.get (id = id)
        task.status = "2"
        task.save()
        messages.success (request, "%s одобрена." % id)
        Notification.generate_accept (task)
        return http.HttpResponseRedirect('../../?%s' % self.get_params(request))

    def to_draft (self, request, id):
        #Task.objects.filter (id = id).update (status = "4")
        messages.success (request, "%s отклонена." % id)
        task = Task.objects.get (id = id)
        task.status = "4"
        task.save()
        Notification.generate_reject (task)
        return http.HttpResponseRedirect('../../?%s' % self.get_params(request))

    def close (self, request, id):
        Task.objects.filter (id = id).update (status = "3")
        messages.success (request, "%s закрыта." % id)
        return http.HttpResponseRedirect('../../?%s' % self.get_params(request))

admin.site.register (Task, TaskAdmin)
