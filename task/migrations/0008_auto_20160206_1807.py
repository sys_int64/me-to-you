# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-06 18:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0007_auto_20160206_1725'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='category',
            field=models.CharField(choices=[('2', 'Уборка и помощь по хозяйству'), ('10', 'Ремонт цифровой техники'), ('12', 'Красота и здоровье'), ('8', 'Web-разработка'), ('13', 'Юридическая помощь'), ('6', 'Удаленная работа'), ('9', 'Фото- и видео-услуги'), ('1', 'Курьерские услуги'), ('7', 'Дизайн'), ('4', 'Компьютерная помощь'), ('5', 'Мероприятия и промо-акции'), ('3', 'Грузоперевозки'), ('0', 'Бытовой ремонт'), ('11', 'Установка и ремонт техники')], default='0', max_length=2, verbose_name='Категория'),
        ),
        migrations.AlterField(
            model_name='task',
            name='category',
            field=models.CharField(choices=[('2', 'Уборка и помощь по хозяйству'), ('10', 'Ремонт цифровой техники'), ('12', 'Красота и здоровье'), ('8', 'Web-разработка'), ('13', 'Юридическая помощь'), ('6', 'Удаленная работа'), ('9', 'Фото- и видео-услуги'), ('1', 'Курьерские услуги'), ('7', 'Дизайн'), ('4', 'Компьютерная помощь'), ('5', 'Мероприятия и промо-акции'), ('3', 'Грузоперевозки'), ('0', 'Бытовой ремонт'), ('11', 'Установка и ремонт техники')], default='0', max_length=2, verbose_name='Категория'),
        ),
    ]
