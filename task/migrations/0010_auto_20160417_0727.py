# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-17 07:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0009_auto_20160301_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='sub_category',
            field=models.CharField(default='0', max_length=2, verbose_name='Под категория'),
        ),
        migrations.AlterField(
            model_name='category',
            name='category',
            field=models.CharField(choices=[('9', 'Фото- и видео-услуги'), ('10', 'Ремонт цифровой техники'), ('12', 'Красота и здоровье'), ('1', 'Курьерские услуги'), ('11', 'Установка и ремонт техники'), ('5', 'Мероприятия и промо-акции'), ('6', 'Удаленная работа'), ('7', 'Дизайн'), ('13', 'Юридическая помощь'), ('0', 'Бытовой ремонт'), ('2', 'Уборка и помощь по хозяйству'), ('8', 'Web-разработка'), ('4', 'Компьютерная помощь'), ('3', 'Грузоперевозки')], default='0', max_length=2, verbose_name='Категория'),
        ),
        migrations.AlterField(
            model_name='task',
            name='category',
            field=models.CharField(choices=[('9', 'Фото- и видео-услуги'), ('10', 'Ремонт цифровой техники'), ('12', 'Красота и здоровье'), ('1', 'Курьерские услуги'), ('11', 'Установка и ремонт техники'), ('5', 'Мероприятия и промо-акции'), ('6', 'Удаленная работа'), ('7', 'Дизайн'), ('13', 'Юридическая помощь'), ('0', 'Бытовой ремонт'), ('2', 'Уборка и помощь по хозяйству'), ('8', 'Web-разработка'), ('4', 'Компьютерная помощь'), ('3', 'Грузоперевозки')], default='0', max_length=2, verbose_name='Категория'),
        ),
    ]
