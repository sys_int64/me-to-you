import mptt
from django.db import models
import datetime
from django.contrib.auth.models import User

from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from location_field.models.spatial import LocationField
from django.utils import formats
from sorl.thumbnail import ImageField
#from location_field.models.plain import PlainLocationField

MARKER_ICONS_CHOICES = (
    ('0' , 'Cargo'  ), ('1' , 'Cleaning'), ('2' , 'Comp help'),
    ('3' , 'Courier'), ('4' , 'Design'  ), ('5' , 'Health'   ),
    ('6' , 'Jury'   ), ('7' , 'Mobile'  ), ('8' , 'Panda'    ),
    ('9' , 'Photo'  ), ('10', 'Wifi'    ), ('11', 'Services' ),
    ('12', 'Wash'   ), ('13', 'Web'     ),
)

"""
class TaskCategory (models.Model):
    parent = models.ForeignKey ('self'    , blank = True, null = True, verbose_name = "Родитель", related_name = 'child')
    marker = models.CharField  ("Маркер"  , max_length = 2, choices = MARKER_ICONS_CHOICES, default = '0')
    title  = models.CharField  ("Название", max_length = 200)
    icon   = models.ImageField ("Иконка"  , blank = True)
    bg     = models.ImageField ("Фон"     , blank = True)

    def __str__ (self):
        #return self.title
        return '%s%s' % ('-' * self.level, self.title)
"""

TASK_STATUS_CHOICES = (
    ('0', 'Новая'    ), ('1', 'Модерация'), ('2', 'Открыто' ),
    ('3', 'Закрыто'  ), ('4', 'Черновик' ), ('5', 'В работе'),
    ('6', 'Выполнено'),
)

CATEGORY_CHOICES = {
    ('0' , 'Бытовой ремонт'      ), ('1' , 'Курьерские услуги'      ), ('2' , 'Уборка и помощь по хозяйству'),
    ('3' , 'Грузоперевозки'      ), ('4' , 'Компьютерная помощь'    ), ('5' , 'Мероприятия и промо-акции'   ),
    ('6' , 'Удаленная работа'    ), ('7' , 'Дизайн'                 ), ('8' , 'Web-разработка'              ),
    ('9' , 'Фото- и видео-услуги'), ('10', 'Ремонт цифровой техники'), ('11', 'Установка и ремонт техники' ),
    ('12', 'Красота и здоровье'  ), ('13', 'Юридическая помощь'     ), ('14', 'Пошив и ремонт одежды и аксессуаров'),
}

from django.core.urlresolvers import reverse

class Task (models.Model):

    class Meta:
        verbose_name        = 'Задачу'
        verbose_name_plural = 'Задачи'

    user         = models.ForeignKey           (User, related_name='owner')
    perfomer     = models.ForeignKey           (User, null = True, blank = True, related_name='perfomer')
    title        = models.CharField            ("Название"    , max_length = 200)
    #categories  = models.ForeignKey           (TaskCategory  , blank = True, null = True, verbose_name = "Категория")
    category     = models.CharField            ("Категория"   , max_length = 2, choices = CATEGORY_CHOICES, default = '0')
    sub_category = models.CharField            ("Под категория", max_length = 2, default = '0')
    desc         = models.TextField            ("Подробнее"   , blank = True)
    price        = models.PositiveIntegerField ("Стоимость"   , blank = True)
    date_time    = models.DateTimeField        ("Время и дата", blank = True, default = datetime.datetime.now)
    life_time    = models.DateTimeField        ("Время жизни" , blank = True, default = datetime.datetime.now)
    address      = models.CharField            ("Адрес"       , blank = True, max_length = 255, default = "Новосибирск")
    location     = LocationField               (based_fields=[address], zoom=7, default='Point(55.018803 82.933952)')
    phone        = models.CharField            ("Телефон"     , max_length = 50 , null    = True)
    status       = models.CharField            ("Статус"      , max_length = 1  , choices = TASK_STATUS_CHOICES, default = '0')
    last_status  = models.CharField            ("Статус2"     , max_length = 1  , choices = TASK_STATUS_CHOICES, default = '0')
    completed    = models.BooleanField         (default = False)
    #photo       = models.ImageField           ("Фото"        , blank = True)
    objects      = models.GeoManager()

    #def actions_button (self, request, extra_context=None):

    #    print (self.get_absolute_url())
    #    if (self.status == "1"):
    #        return '<a href="accept/%s%s">Одобрить</a>' % (self.id, obj.url_field)
    #        #return '<form action = "accept/" method = "post"><input type = "submit" value = "Одобрить"></form> '

    #    return ""

    #actions_button.allow_tags = True
    #actions_button.short_description = 'Действие'

    def _serialize_photos (self):
        photos = []

        for photo in self.photo_set.all():
            url  = "{0}{1}".format ("http://me2you.pro", photo.src.url) if photo.src else ""
            photos.append (url)

        return photos

    def serialize (self, extend = False, user_extend = False, myuser = None):
        user     = self.user
        perfomer = self.perfomer
        #photo  = "{0}{1}".format ("http://me2you.pro", self.photo.url) if self.photo else ""
        user_content = user.serialize (extend = user_extend) if user_extend else user.id

        if (perfomer):
            prefomer_content = perfomer.serialize (extend = user_extend) if user_extend else perfomer.id
        else:
            prefomer_content = None

        return {
            'id'             : self.id,
            'user'           : user_content,
            'perfomer'       : prefomer_content,
            'photos'         : self._serialize_photos(),
            'title'          : self.title,
            'category'       : int(self.category),
            'desc'           : self.desc,
            'price'          : self.price,
            'date_time'      : formats.date_format (self.date_time, "DATETIME_FORMAT"),
            'life_time'      : formats.date_format (self.life_time, "DATETIME_FORMAT"),
            'address'        : self.address,
            'phone'          : self.phone,
            'status'         : int(self.status),
            'lat'            : self.location[1],
            'lng'            : self.location[0],
            'my'             : myuser == self.user,
            'no_fact_addr'   : self.address == "",
            'contract_price' : self.price == 0,
            'completed'      : task.completed,
        }

    #def save (self, *args, **kwargs):
    #    super(Task, self).save(*args, **kwargs)

    #    if (self.last_status == self.status):
    #        return

    #    self.last_status = self.status
    #    self.save()

        #if (self.status == "2"):
        #    NotificationFactory.generate_accept (self)

        #if (self.status == "4"):
        #    NotificationFactory.generate_reject (self)

    def _serialize_short (self):
        pass

    def _serialize_extend (self):
        pass

    def __str__ (self):
        return self.title

class Photo (models.Model):

    class Meta:
        verbose_name        = 'Фотография'
        verbose_name_plural = 'Фотографии'

    task  = models.ForeignKey (Task)
    src   = ImageField ("Фото" , blank = True)
#    thumb = models.ImageField ("Thumb", blank = True)

class Category (models.Model):

    class Meta:
        verbose_name        = 'Подписка'
        verbose_name_plural = 'Подписки'

    user     = models.ForeignKey (User)
    category = models.CharField ("Категория", max_length = 2, choices = CATEGORY_CHOICES, default = '0')

#mptt.register (TaskCategory,)
