"""

"""

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ')yj7v%+sli&t5ac%8&$$zxf^^97s^9qk32uyisld%@-7k7+25v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['bizzi.pro']

STATIC_ROOT = '/home/dev/vwrapperhome/static_content/static/'
STATIC_URL  = '/static/'

MEDIA_ROOT  = '/home/dev/vwrapperhome/static_content/media/'
MEDIA_URL   = '/media/'

SOCIAL_AUTH_FACEBOOK_KEY    = "1043862075646798"
SOCIAL_AUTH_FACEBOOK_SECRET = "a81cf24e1aa55e529f377c4e8ca19182"

SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'locale': 'ru_RU',
    #'fields': 'id, name, email, gender, location, picture.type(large)'
    'fields': 'id, name, gender, location, picture.type(large)'
}

SOCIAL_AUTH_VK_OAUTH2_KEY        = '5092473'
SOCIAL_AUTH_VK_OAUTH2_SECRET     = 'cH2kv1UvMTy9YgUO4Opn'

SOCIAL_AUTH_VK_OAUTH2_EXTRA_DATA = ['city', 'photo_200', 'sex']
#SOCIAL_AUTH_FACEBOOK_SCOPE       = ['email', 'user_location', 'public_profile']
SOCIAL_AUTH_FACEBOOK_SCOPE       = ['user_location', 'public_profile']

#SOCIAL_AUTH_GOOGLE_OAUTH2_IGNORE_DEFAULT_SCOPE = True
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '539951452623-ihrrpg8m4ff8lb9j2b49ptmd85hkqvpl.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'WqQsYWLoXa7C1up1FNEr-yl-'
SOCIAL_AUTH_GOOGLE_OAUTH2_USE_UNIQUE_USER_ID = True
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/userinfo.profile'
]
#SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = ['profile', 'login']

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

#GOOGLE_OAUTH2_CLIENT_ID = "981771903942-95gt9h6n28ctqh865o5sjbrdrtg9869q.apps.googleusercontent.com"
#GOOGLE_OAUTH2_CLIENT_SECRET = "K_hqU7zwXEWK2rcSTAa0xODj"

#SOCIAL_AUTH_GOOGLE_OAUTH2_KEY    = "981771903942-41sdjk75kij42v5hdrpiijaoohi3q120.apps.googleusercontent.com"
#SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = "IK66XUOqse-s0976objSspMD"

# django
# django-flat-theme
# oauth2_provider
# djangorestframework
# markdown
# django-filter
# feincms
# django-rest-framework-social-oauth2
# Pillow
# django-cors-headers
# python-social-auth
# django-location-field
# requests
# gunicorn

INSTALLED_APPS = (
    'flat', # +
    'gunicorn',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'location_field', # -
    'sorl.thumbnail', # -

    # AUTH
    'oauth2_provider', # +
    'social.apps.django_app.default', # +
    'rest_framework_social_oauth2', # +
    'corsheaders', # +
    'rest_framework', # +
    'backends',
    'feincms', # +

    # Models
    'task',
    'userprofile',
    'notification',
    'review',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'social.apps.django_app.middleware.SocialAuthExceptionMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'userprofile.update_shows.UpdateShowsMiddleware',
)

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.user.create_user',
    'userprofile.pipeline.save_profile',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
)

ROOT_URLCONF = 'metoyou.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), os.path.join(BASE_DIR, 'api/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'metoyou.wsgi.application'

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'm2y_db',
        'USER': 'dev',
        'PASSWORD': 'iS7m$A2O##v373RG3Swr',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
#50770.local

LANGUAGE_CODE = 'ru-RU'
TIME_ZONE     = 'UTC'
USE_I18N      = True
USE_L10N      = False
USE_TZ        = True

AUTHENTICATION_BACKENDS = (

    # Social OAuth2 Backends
    'social.backends.google.GoogleOAuth2',
    'backends.vk.VKOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    #'social.backends.google.GooglePlusAuth',

    # Rest OAuth2 backend
    #'rest_framework_social_oauth2.backends.DjangoOAuth2',

    # Django
    'django.contrib.auth.backends.ModelBackend',
    'backends.email.EmailAuthBackend',
)

DATETIME_FORMAT  = "d.m.Y (H:i)"
DATE_FORMAT      = "d.m.Y"
TIME_FORMAT      = "H:i"
JAVA_TIME_FORMAT = "HH:mm"

SOCIAL_AUTH_URL_NAMESPACE = 'social'
REST_FRAMEWORK = {

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'oauth2_provider.ext.rest_framework.OAuth2Authentication',
        'rest_framework_social_oauth2.authentication.SocialAuthentication',
    ),

    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),

    'EXCEPTION_HANDLER': 'rest_framework.views.exception_handler'

}

TEMPLATE_CONTEXT_PROCESSORS = (
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)

OAUTH2_PROVIDER = {
    'SCOPES': {'read': 'Read scope', 'write': 'Write scope', 'groups': 'Access to your groups'}
}

DEBUG_THUMBNAILS = True
