"""metoyou URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""

from django.contrib             import admin
from django.conf.urls           import include, url
from django.contrib.auth.models import User, Group
from userprofile.models         import UserProfile
#import .views
from . import views

from django.conf import settings
from django.conf.urls.static import static

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import staticfiles
from django.core.exceptions import ImproperlyConfigured

admin.autodiscover()

from rest_framework                     import permissions, routers, serializers, viewsets
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
import datetime

router = routers.DefaultRouter()

#def static(prefix, view=staticfiles.views.serve, **kwargs):
#    if not settings.DEBUG or (prefix and '://' in prefix):
#        return []
#    elif not prefix:
#        raise ImproperlyConfigured("Empty static prefix not permitted")
#    return [
#        url(r'^%s(?P<path>.*)$' % re.escape(prefix.lstrip('/')), view, kwargs=kwargs),
#    ]

urlpatterns = [

    # Home Page
    url(r'^$'      , views.index),
    url(r'^terms$', views.terms),

    # Auth
    url(r'^o/'    , include ('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^auth/' , include ('rest_framework_social_oauth2.urls')),

    #
    #url(r'^'      , include (router.urls)),
    url(r'^api/'  , include ('api.urls', namespace = "api")),
    url(r'^admin/', include (admin.site.urls)),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#urlpatterns += staticfiles_urlpatterns()
