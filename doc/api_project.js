define({
  "title": "Документация по REST API сервиса Me to You",
  "url": "http://bizzi.pro/api",
  "name": "Me to You API",
  "version": "1.0.0",
  "description": "Документация по REST API сервиса Me to You",
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-07-05T04:57:56.400Z",
    "url": "http://apidocjs.com",
    "version": "0.15.1"
  }
});
