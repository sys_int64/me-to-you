define({ "api": [
  {
    "type": "post",
    "url": "/user/convert-token/",
    "title": "Преобразовине Access Token",
    "name": "ConvertAccessToken",
    "group": "AccessToken",
    "description": "<p>Преобразовывает полученный Access Token с другого сервиса, к примеру с VK, в Access Token <br> Me to You. grant_type должен быть равен convert_token<br> <br> Поля client_id, client_id - обязательные</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grant_type",
            "description": "<p>Тип запроса:<br>    <b>convert_token</b> - Преобразование токена от другого сервиса в<br>                                        Access Token сервиса Me to You</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_id",
            "description": "<p>Уникальный номер клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "backend",
            "description": "<p>Backend сервиса:<br>    <b>vk-oauth2</b> - Vkontakte<br>    <b>facebook</b> - Facebook<br>    <b>google-oauth2</b> - Google+</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Токен, который необходимо преобразовать</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Тип токена</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "expires_in",
            "description": "<p>Время жизни токена</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>Токен для обновления Access Token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Ключи для авторизации</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "scope",
            "description": "<p>Права</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный запрос:",
          "content": "HTTP/1.1 200 OK\n{\n    \"token_type\": \"Bearer\",\n    \"expires_in\": 36000,\n    \"refresh_token\": \"QNTSeIxuR5S0jSuZ5Gyt2jhWN6upRC\",\n    \"access_token\": \"oBxiT6l8v4HdbmNRe3EwZki5CY7ay2\",\n    \"scope\": \"write read groups\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api/oauth2/convert-token.py",
    "groupTitle": "AccessToken"
  },
  {
    "type": "post",
    "url": "/user/token/",
    "title": "Получение Access Token",
    "name": "GetAccessToken",
    "group": "AccessToken",
    "description": "<p>Метод для получение Access Token. Есть три способа получить Access Token: через ввод пары логин/пароль; через refresh_token для обновление уже имеющегося Access Token, у которого истек срок жизни; <a href = \"#api-AccessToken-ConvertAccessToken\">преобразовать Access Token</a> стороннего сервиса (к примеру VK) в токен приложение.<br> <br> при <b>grant_type</b> = password необходимо передать username; password;<br> при <b>grant_type</b> = refresh_token необходимо передать серверу refresh_token;<br> при <b>grant_type</b> = convert_token необходимо передать серверу backend; token<br> <br> Поля client_id, client_id - обязательные</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grant_type",
            "description": "<p>Тип запроса:<br>    <b>password</b> - Получение токена по паре лоигн/пароль,<br>    <b>refresh_token</b> - Обновление Access Token,<br>    <b>convert_token</b> - Преобразование токена от другого сервиса в<br>                                        Access Token сервиса Me to You</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_id",
            "description": "<p>Уникальный номер клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Имя пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>Токен для обновления существующего токена</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Тип токена</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "expires_in",
            "description": "<p>Время жизни токена</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>Токен для обновления Access Token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Ключи для авторизации</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "scope",
            "description": "<p>Права</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный запрос:",
          "content": "HTTP/1.1 200 OK\n{\n    \"token_type\": \"Bearer\",\n    \"expires_in\": 36000,\n    \"refresh_token\": \"QNTSeIxuR5S0jSuZ5Gyt2jhWN6upRC\",\n    \"access_token\": \"oBxiT6l8v4HdbmNRe3EwZki5CY7ay2\",\n    \"scope\": \"write read groups\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api/oauth2/token.py",
    "groupTitle": "AccessToken"
  },
  {
    "type": "post",
    "url": "/o/revoke_token/",
    "title": "Аннулирование токена",
    "name": "RevokeAccessToken",
    "group": "AccessToken",
    "description": "<p>Удаление токена - выход из системы.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_id",
            "description": "<p>Уникальный номер клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Токен, который необходимо удалить</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Тип токена</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "expires_in",
            "description": "<p>Время жизни токена</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>Токен для обновления Access Token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Ключи для авторизации</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "scope",
            "description": "<p>Права</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/oauth2/revoke-token.py",
    "groupTitle": "AccessToken"
  },
  {
    "type": "errors",
    "url": "/",
    "title": "Коды ошибок Task",
    "name": "TaskErrors",
    "group": "Errors",
    "description": "<style>pre.language-html[data-type=\"errors\"]:before {background-color: #ed0039;}</style>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "T0",
            "description": "<p>Задача не найдена</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "T1",
            "description": "<p>Пользователь не является собственником задачи</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "T2",
            "description": "<p>Статус задачи должен быть <code>В работе</code></p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "T3",
            "description": "<p>Задание уже выполнено</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "T4",
            "description": "<p>Пользователь не является исполнителем для данной задачи</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/errors.py",
    "groupTitle": "Errors"
  },
  {
    "type": "errors",
    "url": "/",
    "title": "Коды ошибок User",
    "name": "UserErrors",
    "group": "Errors",
    "description": "<style>pre.language-html[data-type=\"errors\"]:before {background-color: #ed0039;}</style>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U0",
            "description": "<p>Пользователь не найден</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U1",
            "description": "<p>Пользователь уже активирован</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U2",
            "description": "<p>Проверочный ключ не действителен, обновите ключ</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U3",
            "description": "<p>Ключ введен не верно</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U4",
            "description": "<p>Неверно введен старый пароль</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U5",
            "description": "<p>Пользователь с таким телефоном уже зарегистрирован в системе</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U6",
            "description": "<p>Неверный хост для Digits</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U7",
            "description": "<p>Ошибка при подтверждении секретного кода от Digits, возвращает так же <code>twitter_code</code> вместе с ошибкой</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U8",
            "description": "<p>Не удалось обновить/подтвердить телефон</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U9",
            "description": "<p>У пользователя уже установлена роль</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U10",
            "description": "<p>Неизвестаная роль для пользователя</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U11",
            "description": "<p>Оперция не позволеня для пользователя зарегистрированного через социальную сеть</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U12",
            "description": "<p>Пользователь не может размещать задание с не подтвержденным номером телефона</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U13",
            "description": "<p>Исчерпан лимит на показ телефонов для задания</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "U14",
            "description": "<p>Пользователь с таким e-mail уже зарегистрирован в системе</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/errors.py",
    "groupTitle": "Errors"
  },
  {
    "type": "post",
    "url": "/notifications/dismiss/",
    "title": "Удалить уведомление",
    "name": "DismissNotificationCount",
    "group": "Notification",
    "description": "<p>Удаляет уведомление у пользователя.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Идентификатор уведомления</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ok",
            "description": "<p><code>true</code> если все хорошо</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/notification/dismiss.py",
    "groupTitle": "Notification"
  },
  {
    "type": "post",
    "url": "/notifications/",
    "title": "Отображает уведомления пользователя",
    "name": "Notifications",
    "group": "Notification",
    "description": "<p>Возвращает массив уведомлений</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Идентификатор уведомления</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Заголовок</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "body",
            "description": "<p>Содержаение уведомления</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "system",
            "description": "<p><code>true</code> если уведомление системное</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "datetime",
            "description": "<p>Время когда пришло уведомление</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "android_format",
            "description": "<p>Формат времени для android <br>    <code>dd.MM (HH:mm)</code> - Если уведомление пришло не сегодня <br>    <code>HH:mm</code> - Если уведомление пришло сегодня \\</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Статус уведомления:<br>    <b>0</b> - Не задан<br>    <b>1</b> - Одобрен<br>    <b>2</b> - Отклонен<br>    <b>3</b> - Взять заказ<br>    <b>4</b> - Оценить<br>    <b>5</b> - Новое задание</p>"
          },
          {
            "group": "Success 200",
            "type": "[User]",
            "optional": false,
            "field": "user",
            "description": "<p>Краткое содержание <a href = \"#api-User-GetUser\">пользователя</a>, отображается только поля <code>id</code>; <code>fullname</code>; <code>avatar</code> поле <code>fullname</code> = <code>first_name</code>+&quot; &quot;+<code>last_name</code>. Либо <code>null</code>, если пользователь не указан.</p>"
          },
          {
            "group": "Success 200",
            "type": "[Task]",
            "optional": false,
            "field": "task",
            "description": "<p>Краткое содержание <a href = \"#api-Task-GetTask\">задачи</a>, отображается только поля <code>id</code>; <code>title</code>; <code>category</code>; <code>my</code>; <code>status</code>; <code>no_fact_addr</code>. Либо <code>null</code>, если пользователь не указан.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/notification/details.py",
    "groupTitle": "Notification"
  },
  {
    "type": "post",
    "url": "/notifications/count/",
    "title": "Количество уведомлений",
    "name": "NotificationsCount",
    "group": "Notification",
    "description": "<p>Метод возвращает количество уведомлений пользователя.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>Количество уведомлений</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/notification/details.py",
    "groupTitle": "Notification"
  },
  {
    "type": "get",
    "url": "/reviews[:offset,:limit]/",
    "title": "Отобразить отзывы пользователя",
    "name": "Reviews",
    "group": "Review",
    "description": "<p>Поля <code>:offset</code>; <code>:limit</code> являются не обязательными.<br> Отображает уведомления для пользователя с идентификатором <code>user_id</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Идентификатор пользователя, если <code>user_id</code> = <code>my</code>, то отобразить пользователя по авторизованному токену</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "about_me",
            "description": "<p>Отобразить отзывы о пользователе, если <code>true</code></p>"
          },
          {
            "group": "Parameter",
            "type": "[Review]",
            "optional": false,
            "field": "reviews",
            "description": "<p>Отзывы пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Идентификатор пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date_time",
            "description": "<p>Дата и время отзыва</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Отзыв</p>"
          },
          {
            "group": "Parameter",
            "type": "[User]",
            "optional": false,
            "field": "user",
            "description": "<p>Краткое содержание <a href = \"#api-User-GetUser\">пользователя</a>, отображается только поля <code>id</code>; <code>rank</code>; <code>fullname</code>; <code>avatar</code> поле <code>fullname</code> = <code>first_name</code>+&quot; &quot;+<code>last_name</code>. Либо <code>null</code>, если пользователь не указан.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Task]",
            "optional": false,
            "field": "task",
            "description": "<p>Краткое содержание <a href = \"#api-Task-GetTask\">задачи</a>, отображается только поля <code>id</code>; <code>title</code>. Либо <code>null</code>, если пользователь не указан.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/review/details.py",
    "groupTitle": "Review"
  },
  {
    "type": "post",
    "url": "/review/write/",
    "title": "Написать отзыв",
    "name": "WriteReview",
    "group": "Review",
    "description": "<p>Написать отзыв пользователю с <code>id</code> = <code>to_id</code>, от пользователя с <code>id</code> = <code>user_id</code> за выполнение задачи с <code>id</code> = <code>task_id</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Идентификатор автора отзыва</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "to_id",
            "description": "<p>Идентификатор пользователя, кому отсылается отзыв</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "task_id",
            "description": "<p>Идентификатор задачи, привязанной к отзыву</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rank",
            "description": "<p>Рейтинг</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Отзыв к заданию</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "ok",
            "description": "<p><code>true</code> если все хорошо</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/review/write.py",
    "groupTitle": "Review"
  },
  {
    "type": "post",
    "url": "/task/cancel/:id/",
    "title": "Отмена задачи",
    "name": "CancelTask",
    "group": "Task",
    "description": "<p>Отмена задания с <code>id</code> = <code>:id</code></p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":id",
            "description": "<p>Уникальный идентификатор задачи</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/cancel.py",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/close/:id/",
    "title": "Отметить как выполненное",
    "name": "CloseTask",
    "group": "Task",
    "description": "<p>Отметить задания с <code>id</code> = <code>:id</code> как выполненное.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":id",
            "description": "<p>Уникальный идентификатор задачи</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/close.py",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/complete/:id/",
    "title": "Сообщить о выполнении",
    "name": "CompleteTask",
    "group": "Task",
    "description": "<p>Исполнитель сообщает о выполнении задания с <code>id</code> = <code>:id</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":id",
            "description": "<p>Уникальный идентификатор задачи</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/complete.py",
    "groupTitle": "Task"
  },
  {
    "type": "get",
    "url": "/tasks/filter/[:offset,:limit]/:params/",
    "title": "Фильтр задач",
    "name": "FilterTask",
    "group": "Task",
    "description": "<p><code>:offset</code>; <code>:limit</code>; <code>:params</code> являются не обязательными полями.<br> Фильтрует задачи. Если фильтровать только мои задачи, то в cats вписываются значения <code>status</code>, по которым фильтровать <a href = \"#api-Task-GetTask\">задачи</a>. Иначе фитрация по категориям.<br> Примеры запросов:<br>    <code>/tasks/filter/?cats=1,2,3&amp;city=Новосибирск</code> - Отфильтровать записи, и отобразить все отфильтрованные записи.<br>    <code>/tasks/filter[0:10]/?cats=1,2,3&amp;city=Новосибирск</code> - Отфильтровать записи, и отобразить с 0 по 10 записи.<br>    <code>/tasks/filter,short/?cats=1,2,3&amp;city=Новосибирск</code> - Отфильтровать записи, и отобразить все отфильтрованные записи в кратком содержании.<br>    <code>/tasks/filter,short/my/?cats=1,2,3&amp;city=Новосибирск</code> - Отфильтровать мои записи, и отобразить все отфильтрованные записи в кратком содержании.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Количество пропущенных задач</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Максимальное количество задач</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "params",
            "description": "<p>Параметры отображения, через запятую:<br>    <b>my</b> - фильтровать только задачи авторизованного пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cats",
            "description": "<p>По каким категориям показывать задачи, строка, через запятую, пример: <code>1,3,6</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>Город, в котором находятся маркеры</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Task]",
            "optional": false,
            "field": "tasks",
            "description": "<p>Список найденых <a href = \"#api-Task-GetTask\">задач</a></p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "{\n    \"tasks\": [\n        {\n            \"perfomer\": null,\n            \"lng\": 0.0,\n            \"lat\": 0.0,\n            \"category\": 7,\n            \"photos\": [\n                {\n                    \"thumb\": \"http://me2you.pro/media/uploaded_image1455351818_1100883_kdFsne8.jpg\",\n                    \"src\": \"http://me2you.pro/media/uploaded_image1455351817_9023857.jpg\"\n                }\n            ],\n            \"my\": false,\n            \"phone\": \"\",\n            \"id\": 20,\n            \"user\": 25,\n            \"status\": 2,\n            \"title\": \"создать дизайн для сайта\",\n            \"price\": 25000,\n            \"contract_price\": false,\n            \"desc\": \"срочно\",\n            \"address\": \"\",\n            \"no_fact_addr\": true,\n            \"life_time\": \"22.02.2016 (14:25)\",\n            \"date_time\": \"22.02.2016 (14:23)\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/filter.py",
    "groupTitle": "Task"
  },
  {
    "type": "get",
    "url": "/task/:id,:params/",
    "title": "Информация о задаче",
    "name": "GetTask",
    "group": "Task",
    "description": "<p>Получение информации о задаче по уникальному идентификатору :id с параметрами :params.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "params",
            "description": "<p>Параметры отображения, через запятую:<br>    <b>user_detail</b> - отображение подробной информации об авторе задачи и исполнителе</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор задачи</p>"
          },
          {
            "group": "Success 200",
            "type": "User",
            "optional": false,
            "field": "user",
            "description": "<p>Уникальный идентификатор автора задачи или объект <a href = \"#api-User-GetUser\">User</a>, если включен параметр user_detail</p>"
          },
          {
            "group": "Success 200",
            "type": "User",
            "optional": false,
            "field": "perfomer",
            "description": "<p>Уникальный идентификатор исполнителя задачи или объект <a href = \"#api-User-GetUser\">User</a>, если включен параметр user_detail</p>"
          },
          {
            "group": "Success 200",
            "type": "[Object]",
            "optional": false,
            "field": "photos",
            "description": "<p>Список фотографий, <code>thumb</code> - миниатюра фотографии, <code>src</code> - полный размер фотографии</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Заголовок</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Описание задачи</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Цена</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Широта, где распологается маркер</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Долгота, где распологается маркер</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>Номер категории:<br>    <b>0</b> - Бытовой ремонт<br>    <b>1</b> - Курьерские услуги<br>    <b>2</b> - Уборка и помощь по хозяйству<br>    <b>3</b> - Грузоперевозки<br>    <b>4</b> - Компьютерная помощь<br>    <b>5</b> - Мероприятия и промо-акции<br>    <b>6</b> - Удаленная работа<br>    <b>7</b> - Дизайн<br>    <b>8</b> - Web-разработка<br>    <b>9</b> - Фото- и видео-услуги<br>    <b>10</b> - Ремонт цифровой техники<br>    <b>11</b> - Установка и ремонт техники<br>    <b>12</b> - Красота и здоровье<br>    <b>13</b> - Юридическая помощь</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sub_category",
            "description": "<p>Номер подкатегории:<br>    <b>0</b> - Другое<br>    <b>Бытовой ремонт</b><br>       <b>1</b> - Мелкий бытовой ремонт<br>       <b>2</b> - Услуги электрика<br>       <b>3</b> - Услуги сантехника<br>       <b>4</b> - Отделочные работы<br>    <b>Курьерские услуги</b><br>       <b>1</b> - Пеший курьер<br>       <b>2</b> - Курьер с личным автомобилем<br>       <b>3</b> - Покупка и доставка<br>       <b>4</b> - Доставка еды из ресторанов и магазинов<br>    <b>Уборка и помощь по хозяйству</b><br>       <b>1</b> - Лёгкая уборка<br>       <b>2</b> - Генеральная уборка<br>       <b>3</b> - Стирка<br>       <b>4</b> - Уборка мусора<br>    <b>Пошив и ремонт одежды и аксессуаров</b><br>       <b>1</b> - Ремонт обуви<br>       <b>2</b> - Ремонт часов<br>       <b>3</b> - Ремонт одежды<br>       <b>4</b> - Пошив одежды<br>    <b>Грузоперевозки</b><br>       <b>1</b> - Переезды<br>       <b>2</b> - Пассажирские перевозки<br>       <b>3</b> - Междугородные перевозки<br>       <b>4</b> - Услуги грузчиков<br>    <b>Компьютерная помощь</b><br>       <b>1</b> - Ремонт комьютеров и ноутбуков<br>       <b>2</b> - Установки и настройка операционных, систем, программ<br>       <b>3</b> - Удаление вирусов, шифровщиков<br>       <b>4</b> - Настройка Интернета, вай-фай роутеров<br>    <b>Мероприятия и промо-акции</b><br>       <b>1</b> - Раздача промо материалов<br>       <b>2</b> - Промоутер<br>       <b>3</b> - Ведущий, аниматор<br>       <b>4</b> - Расклейщик объявлений<br>    <b>Удаленная работа</b><br>       <b>1</b> - Работа с текстом, копирайтинг, переводы<br>       <b>2</b> - Поиск и обработка информации<br>       <b>3</b> - Работа в Office программах<br>       <b>4</b> - Реклама и продвижение в Интернете<br>    <b>Дизайн</b><br>       <b>1</b> - Логотипы, фирменный стиль<br>       <b>2</b> - Листовки, буклеты, визитки<br>       <b>3</b> - Дизайн сайтов<br>       <b>4</b> - 3d графика, анимация<br>    <b>Web-разработка</b><br>       <b>1</b> - Создание сайта<br>       <b>2</b> - Программирование<br>       <b>3</b> - Вёрстка<br>       <b>4</b> - Разработка программ и приложений<br>    <b>Фото- и видео-услуги</b><br>       <b>1</b> - Фотосъёмка<br>       <b>2</b> - Видеосъёмка<br>       <b>3</b> - Обработка изображений и видео<br>       <b>4</b> - Видеопрезентации, заставки, слайдшоу<br>    <b>Ремонт цифровой техники</b><br>       <b>1</b> - Телефоны и планшеты<br>       <b>2</b> - Аудиотехника<br>       <b>3</b> - Телевизоры и мониторы<br>       <b>4</b> - Автомобильная электроника<br>    <b>Установка и ремонт техники</b><br>       <b>1</b> - Стиральные машины<br>       <b>2</b> - Холодильники и морозильные камеры<br>       <b>3</b> - Электрические плиты и панели<br>       <b>4</b> - Мелкая бытовая техника<br>    <b>Красота и здоровье</b><br>       <b>1</b> - Парикмахерские услуги<br>       <b>2</b> - Маникю и педикюр<br>       <b>3</b> - Косметология и макияж<br>       <b>4</b> - Персональный тренер<br>    <b>Юридическая помощ</b> <br>       <b>1</b> - Юридичевская консультация<br>       <b>2</b> - Составление и проверка договоров<br>       <b>3</b> - Оформление документов<br>       <b>4</b> - Частный детектив</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "date_time",
            "description": "<p>Когда необходимо выполнить</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "life_time",
            "description": "<p>Время жизни задачи</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Адрес</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Контактный телефон, если пользователь уже интерисовался данным заданием, то отображается телефон, иначе поле будет пустым.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Статус задачи:<br>    <b>1</b> - Модерация<br>    <b>2</b> - Открыто<br>    <b>3</b> - Закрыто<br>    <b>4</b> - Черновик<br>    <b>5</b> - В работе<br>    <b>6</b> - Выполнено</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "contract_price",
            "description": "<p>Цена договорная, если <code>true</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "no_fact_addr",
            "description": "<p>Не требует фактического пребывание, если <code>true</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "completed",
            "description": "<p><code>true</code> если исполнитель отметил задание как выполненное</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "\n{\n    \"perfomer\": 16,\n    \"lng\": 82.944814,\n    \"lat\": 55.024695,\n    \"category\": 9,\n    \"photos\": [\n        {\n            \"thumb\": \"http://me2you.pro/media/uploaded_image1451559422_112213_xRNGZ3Q.jpg\",\n            \"src\": \"http://me2you.pro/media/uploaded_image1451559422_016144.jpg\"\n        }\n    ],\n    \"my\": false,\n    \"phone\": \"\",\n    \"id\": 5,\n    \"user\": 3,\n    \"status\": 6,\n    \"title\": \"сфотать свадьбу\",\n    \"price\": 0,\n    \"contract_price\": true,\n    \"desc\": \"у меня свадьба. нужен оператор\",\n    \"address\": \"Новосибирск, ул. Лескова, 15\",\n    \"no_fact_addr\": false,\n    \"life_time\": \"30.12.2015 (10:01)\",\n    \"date_time\": \"30.12.2016 (10:01)\"\n}",
          "type": "json"
        },
        {
          "title": "Пример c параметром user_detail",
          "content": "{\n    \"perfomer\": {\n        \"my_reviews\": 1,\n        \"rank\": 5,\n        \"birthdate\": \"\",\n        \"phone\": \"79139349968\",\n        \"gender\": \"m\",\n        \"active\": true,\n        \"tasks_count\": 1,\n        \"type\": 2,\n        \"id\": 16,\n        \"about_me_reviews\": 1,\n        \"lastname\": \"Белов\",\n        \"email\": \"cultire@gmail.com\",\n        \"city\": \"Новосибирск\",\n        \"firstname\": \"Александр\",\n        \"skills\": \"\",\n        \"start\": 0,\n        \"avatar\": \"http://me2you.pro/media/uploaded_image1451978125_4634757_1VmcaYO.jpg\",\n        \"subscription\": [],\n        \"date_joined\": \"января 2016\"\n    },\n    \"lng\": 82.944814,\n    \"lat\": 55.024695,\n    \"category\": 9,\n    \"photos\": [\n        {\n            \"thumb\": \"http://me2you.pro/media/uploaded_image1451559422_112213_xRNGZ3Q.jpg\",\n            \"src\": \"http://me2you.pro/media/uploaded_image1451559422_016144.jpg\"\n        }\n    ],\n    \"my\": false,\n    \"phone\": \"\",\n    \"id\": 5,\n    \"user\": {\n        \"my_reviews\": 1,\n        \"rank\": 4,\n        \"birthdate\": \"\",\n        \"phone\": \"79529303040\",\n        \"gender\": \"m\",\n        \"active\": true,\n        \"tasks_count\": 1,\n        \"type\": 1,\n        \"id\": 3,\n        \"about_me_reviews\": 2,\n        \"lastname\": \"Зуев\",\n        \"email\": \"\",\n        \"city\": \"Новосибирск\",\n        \"firstname\": \"Алексей\",\n        \"skills\": \"\",\n        \"start\": 0,\n        \"avatar\": \"http://cs320527.vk.me/v320527547/69ad/XTuwT9-2hCQ.jpg\",\n        \"subscription\": [],\n        \"date_joined\": \"декабря 2015\"\n    },\n    \"status\": 6,\n    \"title\": \"сфотать свадьбу\",\n    \"price\": 0,\n    \"contract_price\": true,\n    \"desc\": \"у меня свадьба. нужен оператор\",\n    \"address\": \"Новосибирск, ул. Лескова, 15\",\n    \"no_fact_addr\": false,\n    \"life_time\": \"30.12.2015 (10:01)\",\n    \"date_time\": \"30.12.2016 (10:01)\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/details.py",
    "groupTitle": "Task"
  },
  {
    "type": "get",
    "url": "/tasks[:offset,:limit]/:params/",
    "title": "Список задач",
    "name": "GetTasks",
    "group": "Task",
    "description": "<p>Получения списка задач. Необязательные параметры :offset, :limit и :params<br> <br> Пример запроса: <code>/tasks/</code><br> Вывести все задачи.<br> <br> Пример запроса: <code>/tasks[0,5]/</code><br> Вывести задачи, максимальное количество задач - 10;<br> <br> Пример запроса: <code>/tasks[5,10]/my/</code><br> Вывести задачи авторизованного пользователя, пропустив первые 5 задач,<br> максимальное количество задач - 10;<br> <br> Пример запроса: <code>/tasks,short/</code><br> Вывести все задачи, с неполными параметрами.<br> С флагом <code>short</code> выводятся только поля: <code>id</code>; <code>status</code>; <code>title</code>; <code>price</code>; <code>my</code>; <code>lat</code>; <code>lng</code>; <code>no_fact_addr</code>; <code>category</code>.<br> <br> Пример запроса: <code>/tasks,short,zip/</code><br> Вывести все задачи в сжатом виде, сжатие через zip, с неполными параметрами.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Количество пропущенных задач</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Максимальное количество задач</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "params",
            "description": "<p>Параметры отображения, через запятую:<br>    <b>my</b> - отображать только задачи авторизованного пользователя</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Task]",
            "optional": false,
            "field": "tasks",
            "description": "<p>Список <a href = \"#api-Task-GetTask\">задач</a></p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zip",
            "description": "<p>Сжатый JSON, список всех задач</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример с флагом \"short\"",
          "content": "{\n    \"tasks\": [\n        {\n            \"status\": 2,\n            \"title\": \"создать дизайн для кластера\",\n            \"price\": 25000,\n            \"my\": false,\n            \"lat\": 0.0,\n            \"no_fact_addr\": true,\n            \"lng\": 0.0,\n            \"id\": 20,\n            \"category\": 7\n        }\n    ]\n}",
          "type": "json"
        },
        {
          "title": "Пример с флагами \"short,zip\"",
          "content": "{\n    \"zip\": \"b'eNo9jD0KwkAQha+y3TYiS0AED+AJ7ETCsIkhGBPZnRQSAia14FVSKP4EPcPsjZyxsPu+ee9NoxH8zuuFWjfaI2AtHE2UxhyLlFmHjj50pysNoQ9nxfBgHehJb5ExXBS9aOS4Cz3dwokGzfuDy63so5kxhn1/ZFlC4VOWApDNTCUoq3gLFmNIEsfHlat/jTL7N/JE/ghZwDSrnLyat5v2C0AlS1k='\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/details.py",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/give-task/:id/",
    "title": "Отдать задачу на исполнение",
    "name": "GiveTask",
    "group": "Task",
    "description": "<p>Отдать задачу с уникальным идентификтором <code>:id</code> на исполнение пользователю с <code>id</code> = <code>perfomer_id</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":id",
            "description": "<p>Уникальный идетификатор задачи</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "perfomer_id",
            "description": "<p>Уникальный идетификатор исполнителя</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "ok",
            "description": "<p><code>true</code> если все хорошо</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/give_task.py",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/place/",
    "title": "Разместить задачу на бирже/черновик",
    "name": "PlaceTask",
    "group": "Task",
    "description": "<p>Размещает задание на бирже или в черновик, если <code>draft</code> = <code>true</code>. Если задача размещается в черновик, сначала она попадает в раздел модерации, после того как модератор проверит задачу, она попадает на биржу.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "draft",
            "description": "<p>Если <code>true</code> помещает задание в черновик</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Заголовок задачи</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>Категория, в которой разместить задание:<br>    <b>0</b> - Бытовой ремонт<br>    <b>1</b> - Курьерские услуги<br>    <b>2</b> - Уборка и помощь по хозяйству<br>    <b>3</b> - Грузоперевозки<br>    <b>4</b> - Компьютерная помощь<br>    <b>5</b> - Мероприятия и промо-акции<br>    <b>6</b> - Удаленная работа<br>    <b>7</b> - Дизайн<br>    <b>8</b> - Web-разработка<br>    <b>9</b> - Фото- и видео-услуги<br>    <b>10</b> - Ремонт цифровой техники<br>    <b>11</b> - Установка и ремонт техники<br>    <b>12</b> - Красота и здоровье<br>    <b>13</b> - Юридическая помощь</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Описание задачи</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Цена</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Адрес задачи</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Широта, по которому распологается маркер</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Долгота, по которому распологается маркер</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date_time",
            "description": "<p>Когда необходимо выполнить, формат: <code>dd.mm.YYYY H:i</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "life_time",
            "description": "<p>Время жизни задачи, формат: <code>dd.mm.YYYY H:i</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String[:n]",
            "optional": false,
            "field": "photo",
            "description": "<p>Фотография в формате base64 с порядковым номером <code>:n</code>, т.е. <code>photo[0]</code> - самая первая фотография</p>"
          },
          {
            "group": "Parameter",
            "type": "String[:n]",
            "optional": false,
            "field": "thumb",
            "description": "<p>Миниатюра в формате base64 с порядковым номером <code>:n</code>, т.е. <code>photo[0]</code> - миниатюра к первй фотографии</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/place.py",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/refuse/:id/",
    "title": "Отказаться от задачи",
    "name": "RefuseTask",
    "group": "Task",
    "description": "<p>Исполнитель отказывается от задания с <code>id</code> = <code>:id</code>. При этом задание попадает в <code>открытые</code>, если время жизни еще не истекло.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":id",
            "description": "<p>Уникальный идентификатор задачи</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/refuse.py",
    "groupTitle": "Task"
  },
  {
    "type": "get",
    "url": "/tasks/search/[:offset,:limit]/:params/",
    "title": "Поиск задач",
    "name": "SearchTask",
    "group": "Task",
    "description": "<p>Ищет задачи с ключем <code>key</code> и со статусом <code>открыто</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Количество пропущенных задач</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Максимальное количество задач</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "params",
            "description": "<p>Параметры отображения, через запятую:<br>    <b>my</b> - отображать только задачи авторизованного пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Ключевые слова для поиска</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Task]",
            "optional": false,
            "field": "tasks",
            "description": "<p>Список найденых <a href = \"#api-Task-GetTask\">задач</a></p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "{\n    \"tasks\": [\n        {\n            \"perfomer\": null,\n            \"lng\": 0.0,\n            \"lat\": 0.0,\n            \"category\": 7,\n            \"photos\": [\n                {\n                    \"thumb\": \"http://me2you.pro/media/uploaded_image1455351818_1100883_kdFsne8.jpg\",\n                    \"src\": \"http://me2you.pro/media/uploaded_image1455351817_9023857.jpg\"\n                }\n            ],\n            \"my\": false,\n            \"phone\": \"\",\n            \"id\": 20,\n            \"user\": 25,\n            \"status\": 2,\n            \"title\": \"создать дизайн для сайта\",\n            \"price\": 25000,\n            \"contract_price\": false,\n            \"desc\": \"срочно\",\n            \"address\": \"\",\n            \"no_fact_addr\": true,\n            \"life_time\": \"22.02.2016 (14:25)\",\n            \"date_time\": \"22.02.2016 (14:23)\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/search.py",
    "groupTitle": "Task"
  },
  {
    "type": "get",
    "url": "/task/show-phone/:id/",
    "title": "Запросить телефон для задания",
    "name": "ShowPhoneTask",
    "group": "Task",
    "description": "<p>Если у пользователя не исчерпался лимит на отображение задач, то для задачи с уникальным идентификтором <code>:id</code> метод вернет номер телефона заказчика и уведомит его о том, что пользователь заинтерисовался заданием.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":id",
            "description": "<p>Уникальный идетификатор задачи</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Номер телефона автора задания</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "{\n    \"phone\": \"+79137698347\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/show_phone.py",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/task/update/:id/",
    "title": "Обновить задание",
    "name": "UpdateTask",
    "group": "Task",
    "description": "<p>Обновление задачи, после обновления задача попадает либо в раздел модерации, либо в черновик, если <code>draft</code> = <code>true</code></p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "draft",
            "description": "<p>Если <code>true</code> помещает задание в черновик</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Заголовок задачи</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>Категория, в которой разместить задание:<br>    <b>0</b> - Бытовой ремонт<br>    <b>1</b> - Курьерские услуги<br>    <b>2</b> - Уборка и помощь по хозяйству<br>    <b>3</b> - Грузоперевозки<br>    <b>4</b> - Компьютерная помощь<br>    <b>5</b> - Мероприятия и промо-акции<br>    <b>6</b> - Удаленная работа<br>    <b>7</b> - Дизайн<br>    <b>8</b> - Web-разработка<br>    <b>9</b> - Фото- и видео-услуги<br>    <b>10</b> - Ремонт цифровой техники<br>    <b>11</b> - Установка и ремонт техники<br>    <b>12</b> - Красота и здоровье<br>    <b>13</b> - Юридическая помощь</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Описание задачи</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Цена</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Адрес задачи</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Широта, по которому распологается маркер</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Долгота, по которому распологается маркер</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date_time",
            "description": "<p>Когда необходимо выполнить, формат: <code>dd.mm.YYYY H:i</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "life_time",
            "description": "<p>Время жизни задачи, формат: <code>dd.mm.YYYY H:i</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String[:n]",
            "optional": false,
            "field": "photo",
            "description": "<p>Фотография в формате base64 с порядковым номером <code>:n</code>, т.е. <code>photo[0]</code> - самая первая фотография</p>"
          },
          {
            "group": "Parameter",
            "type": "String[:n]",
            "optional": false,
            "field": "thumb",
            "description": "<p>Миниатюра в формате base64 с порядковым номером <code>:n</code>, т.е. <code>photo[0]</code> - миниатюра к первй фотографии</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/task/update.py",
    "groupTitle": "Task"
  },
  {
    "type": "post",
    "url": "/user/check-new-tasks/",
    "title": "Проверка новых заданий",
    "name": "CheckNewTasksUser",
    "group": "User",
    "description": "<p>Посылает запрос серверу на проверку новых заданий, если задания есть, то сервером отправляется уведомление пользователю о новых заданиях ключ с помощью метода <a href = \"#api-User-SendCodeUser\">/user/send-code/</a>.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/check_new_tasks.py",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/perfomers[:offset,:limit]/",
    "title": "Список исполнителей",
    "name": "GetPerfomers",
    "group": "User",
    "description": "<p>Отображает список исполнителей в диапазоне [<code>:offset</code>,<code>:limit</code>]</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Количество пропущенных исполнителей</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Максимальное количество исполнителей</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "perfomers",
            "description": "<p>Исполнители</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный номер исполнителя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rank",
            "description": "<p>Рейтинг исполнителя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>Город</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tasks_count",
            "description": "<p>Количество выполненных заданий</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный запрос:",
          "content": "HTTP/1.1 200 OK\n{\n    \"perfomers\": [\n        {\n            \"lastname\": \"Белов\",\n            \"rank\": 5,\n            \"city\": \"Новосибирск\",\n            \"id\": 16,\n            \"avatar\": \"http://me2you.pro/media/uploaded_image1451978125_4634757_1VmcaYO.jpg\",\n            \"tasks_count\": 1,\n            \"firstname\": \"Александр\"\n        },\n        {\n            \"lastname\": \"Зуев\",\n            \"rank\": 5,\n            \"city\": \"Новосибирск\",\n            \"id\": 24,\n            \"avatar\": \"http://me2you.pro/media/uploaded_image1455352754_7142942_n7On8vV.jpg\",\n            \"tasks_count\": 1,\n            \"firstname\": \"Алексей\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/details.py",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/:id/",
    "title": "Инфомация о пользователе",
    "name": "GetUser",
    "group": "User",
    "description": "<p>Получение информации о пользователе по уникальному номеру, либо, если пользователь авторизован, можно получиться данные выполнив запрос <code>/user/my/</code> для получения данных об авторизованном пользователе.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный номер пользователя.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный номер пользователя</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "email",
            "description": "<p>Уникальный номер пользователя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Телефон</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "description": "<p>True, если пользователь активирован</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "skills",
            "description": "<p>Навыки исполнителя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>Пол пользователя: 0 - не задан; m - мужской; а - женский</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "birthdate",
            "description": "<p>Дата рождение. Формат даты: d.m.Y</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>Город</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "social",
            "description": "<p><code>true</code>, если пользователь зашел через соц. сеть</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "avatar",
            "description": "<p>URL аватара пользователя</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>Тип пользователя:<br>    <b>0</b> - Не задан<br>    <b>1</b> - Заказчик<br>    <b>2</b> - Исполнитель</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rank",
            "description": "<p>Рейтинг пользователя</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tasks_count",
            "description": "<p>Количество выполненных заданий для Исполнителя, и количество размещенных заданий для Заказчика</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "date_joined",
            "description": "<p>Дата регистрации на сервисе</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "my_reviews",
            "description": "<p>Количество моих отзывов</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "about_me_reviews",
            "description": "<p>Количество отзывов обо мне</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "subscription",
            "description": "<p>Массив подписанных категорий:<br>    <b>0</b> - Бытовой ремонт<br>    <b>1</b> - Курьерские услуги<br>    <b>2</b> - Уборка и помощь по хозяйству<br>    <b>3</b> - Грузоперевозки<br>    <b>4</b> - Компьютерная помощь<br>    <b>5</b> - Мероприятия и промо-акции<br>    <b>6</b> - Удаленная работа<br>    <b>7</b> - Дизайн<br>    <b>8</b> - Web-разработка<br>    <b>9</b> - Фото- и видео-услуги<br>    <b>10</b> - Ремонт цифровой техники<br>    <b>11</b> - Установка и ремонт техники<br>    <b>12</b> - Красота и здоровье<br>    <b>13</b> - Юридическая помощь</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный запрос:",
          "content": "HTTP/1.1 200 OK\n{\n    \"my_reviews\": 0,\n    \"rank\": 0,\n    \"birthdate\": \"\",\n    \"phone\": \"79137698347\",\n    \"gender\": \"m\",\n    \"active\": true,\n    \"tasks_count\": 0,\n    \"type\": 2,\n    \"id\": 2,\n    \"about_me_reviews\": 0,\n    \"lastname\": \"Кабылин\",\n    \"email\": \"\",\n    \"city\": \"Новосибирск\",\n    \"firstname\": \"Андрей\",\n    \"skills\": \"C/C++; LLVM; ASM;\\nPython; Django.\",\n    \"avatar\": \"http://me2you.pro/media/uploaded_image1455436515_283432_SDVKdAJ.jpg\",\n    \"subscription\": [\n        8,\n        5\n    ],\n    \"date_joined\": \"декабря 2015\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api/user/details.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/recovery-password/",
    "title": "Восстановление пароля",
    "name": "RecoveryPasswordUser",
    "group": "User",
    "description": "<p>Для восстановления пароля через <code>email</code>, для начала необходимо получить защитный ключи с помощью метода <a href = \"#api-User-SendCodeUser\">/user/send-code/</a>, с указанием e-mail и нового пароля, после чего на новый номер отправляется защитный ключ, если пользователь вводит верно, то у пользователя меняется пароль, иначе необходимо снова получить защитный ключ.<br> Если восстановление пароля происходит через Digits, то необходимо задать параметры <code>phone</code>, <code>X-Auth-Service-Provider</code>, <code>X-Verify-Credentials-Authorization</code> для проверки правильности ввода ключа.<br> Подробнее: <a href = \"https://dev.twitter.com/oauth/echo\" target = \"_blank\">https://dev.twitter.com/oauth/echo</a></p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Проверочный ключ</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Телефон</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "X-Auth-Service-Provider",
            "description": "<p>Хост куда отправить</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "X-Verify-Credentials-Authorization",
            "description": "<p>OAuth токены, и сигнатуры для Digits</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/recovery_passwd.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/register/",
    "title": "Регистрация пользователя",
    "name": "RegisterUser",
    "group": "User",
    "description": "<p>Регистрация пользователя. После регистрации автоматически отправляется проверочный код, для подтверждения телефона.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_id",
            "description": "<p>Уникальный номер клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Номер телефона пользователя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail пользователя, не обязательное поле</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "avatar",
            "description": "<p>Картинка в формате base64</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если все регистрация пошла успешно</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный запрос:",
          "content": "HTTP/1.1 200 OK\n{\n    \"ok\": True,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/register.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/send-code/",
    "title": "Отправка кода подтверждения",
    "name": "SendCodeUser",
    "group": "User",
    "description": "<p>Отправка кода с подтверждением на e-mail пользователя.<br> <br> при восстановлении пароля <code>action</code> должно быть равным <code>recovery-password</code>, необходимо заполнить параметры <code>phone</code> номер телефона, который привязан к пользователю; <code>password</code> - новый пароль.<br> <br> Для данного метода обязательно нужна авторизация, за исключением восстановиления пароля.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_id",
            "description": "<p>Уникальный номер клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>Действие</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Новый пароль, при action = <code>recovery-password</code></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный запрос:",
          "content": "HTTP/1.1 200 OK\n{\n    \"ok\": True,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/send_code.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/set-gcm-token/",
    "title": "Сохранение GCM токена",
    "name": "SetGCMTokenUser",
    "group": "User",
    "description": "<p>Сохраняет GCM токен в базе данныз для авторизованного пользователя</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Значение токена</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Успешный запрос:",
          "content": "HTTP/1.1 200 OK\n{\n    \"ok\": True,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/set_gcm_token.py",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/user/set_role/",
    "title": "Задать роль пользователю",
    "name": "SetRole",
    "group": "User",
    "description": "<p>Задает новую роль пользователю.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Роль:<br>    <b>1</b> - Заказчик<br>    <b>2</b> - Исполнитель</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/set_role.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/update-avatar/",
    "title": "Обновление аватара",
    "name": "UpdateAvatarUser",
    "group": "User",
    "description": "<p>Для обновления аватара заполняется параметр base64 - данные картинки в кодировке base64.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "base64",
            "description": "<p>Изображение</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/update_avatar.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/update-password/",
    "title": "Обновление пароля",
    "name": "UpdatePasswordUser",
    "group": "User",
    "description": "<p>Для восстановления пароля, для начала необходимо получить защитный ключи с помощью метода <a href = \"#api-User-SendCodeUser\">/user/send-code/</a>, с указанием номера телефона и нового пароля, после чего на новый номер отправляется защитный ключ, если пользователь вводит верно, то у пользователя меняется пароль, иначе необходимо снова получить защитный ключ.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "old-password",
            "description": "<p>Действующий пароль</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new-password",
            "description": "<p>Новый пароль</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/update_passwd.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/update-phone/",
    "title": "Обновление телефона",
    "name": "UpdatePhoneUser",
    "group": "User",
    "description": "<p>Обновление телефона проиходит через сервис <a href = \"https://get.digits.com/\" target = \"_blank\">Digits</a>. На стороне сервера проверяется правильность ввода проверочного кода через токены и сигнатуры Digits, которые хранятся в поле <code>X-Verify-Credentials-Authorization</code>, подробнее: <a href = \"https://docs.fabric.io/android/digits/verify-user.html#generating-oauth-echo-headers\">Android</a>; <a href = \"https://docs.fabric.io/ios/digits/oauth-echo.html#obtaining-oauth-echo-headers\">iOS</a>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Проверочный ключ</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "X-Auth-Service-Provider",
            "description": "<p>Хост куда отправить</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "X-Verify-Credentials-Authorization",
            "description": "<p>OAuth токены, и сигнатуры для Digits</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/update_phone.py",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/update-profile/",
    "title": "Обновление профильных данных",
    "name": "UpdateProfileUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>Пол пользователя:<br>    <b>0</b> - не задан;<br>    <b>m</b> - мужской;<br>    <b>а</b> - женский</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>Город</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "skills",
            "description": "<p>Навыки исполнителя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthdate",
            "description": "<p>Дата рождения. Формат даты: d.m.Y</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "avatar",
            "description": "<p>Изображение в base64</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/user/update_profile.py",
    "groupTitle": "User"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc/main.js",
    "group": "_home_dev_vwrapperhome_me2you_apidoc_main_js",
    "groupTitle": "_home_dev_vwrapperhome_me2you_apidoc_main_js",
    "name": ""
  }
] });
